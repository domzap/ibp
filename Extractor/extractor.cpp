/**
 * Thesis name: Vehicle Re-identification in Video
 * File name: extractor.cpp
 * Description: This program extracts image parts from video as rectified
 *              vehicles.
 * Written by Dominik Zapletal <xzaple34@stud.fit.vutbr.cz>, May 2015
 */

/*Libraries*/
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/opencv.hpp"
#include <sys/stat.h>
#include <sys/types.h>
#include <iostream>
#include <stdio.h>
#include <cv.h>
#include <fstream>
#include <string>
#include <cstring>
#include <map>

#define ANNOT_LINE_PARTS 17
#define MINIMUM_CAR_REC 10

/*Namespaces*/
using namespace std;
using namespace cv;

/*Global variables*/
vector<float> sameImageVec;
vector<float> diffImageVec;

/**
 * Returns rectangled image (from perspective).
 * @param src
 * @param s output size
 * @param p1
 * @param p2
 * @param p3
 * @param p4
 * @return rectangled image
 */
Mat getRectangledArea(Mat &src, Size s, Point p1, Point p2, Point p3, Point p4) {
    // destination image
    Mat quad = Mat::zeros(s.height, s.width, CV_8UC3);

    // corners
    vector<Point2f> quad_pts;
    quad_pts.push_back(Point2f(0, 0));
    quad_pts.push_back(Point2f(quad.cols, 0));
    quad_pts.push_back(Point2f(quad.cols, quad.rows));
    quad_pts.push_back(Point2f(0, quad.rows));

    vector<Point2f> corners;
    corners.clear();
    corners.push_back(p1);
    corners.push_back(p2);
    corners.push_back(p3);
    corners.push_back(p4);

    // transformation matrix
    Mat transmtx = getPerspectiveTransform(corners, quad_pts);

    // perspective transformation
    warpPerspective(src, quad, transmtx, quad.size());
    return quad;
}

/* simplified Car class*/
class Car {
public:

    int ID;
    int frame;

    // 3D boudning box parts
    Mat front;
    Mat side;
    Mat roof;
    Mat combined;
    Mat squared;

    // 3D BB corners
    Point P[7];
    double sharpness;

public:

    /**
     * Initialization
     */
    Car() : ID(1), frame(0) {
    };

    /**
     * Creates combined image (side + front) and stores it to combined variable.
     */
    void combine() {
        combined = cvCreateMat(front.rows, front.cols + side.cols, front.type());
        Mat tmp = combined(cv::Rect(0, 0, side.cols, side.rows));
        side.copyTo(tmp);
        tmp = combined(cv::Rect(side.cols, 0, front.cols, front.rows));
        front.copyTo(tmp);
    }
};

/**
 * Draws bounding box in the input image. BB is based on P points.
 * @param input
 * @param P
 */
void drawBB(Mat &input, Point * P) {
    line(input, P[0], P[1], Scalar(0, 255, 0), 2, CV_AA, 0);
    line(input, P[1], P[2], Scalar(0, 255, 0), 2, CV_AA, 0);
    line(input, P[2], P[3], Scalar(0, 255, 0), 2, CV_AA, 0);
    line(input, P[3], P[0], Scalar(0, 255, 0), 2, CV_AA, 0);
    line(input, P[0], P[4], Scalar(0, 255, 0), 2, CV_AA, 0);
    line(input, P[4], P[5], Scalar(0, 255, 0), 2, CV_AA, 0);
    line(input, P[5], P[1], Scalar(0, 255, 0), 2, CV_AA, 0);
    line(input, P[5], P[6], Scalar(0, 255, 0), 2, CV_AA, 0);
    line(input, P[6], P[2], Scalar(0, 255, 0), 2, CV_AA, 0);
    // black cornes
    for (int i = 0; i < 7; i++) {
        circle(input, P[i], 5, Scalar(0, 0, 0), -1, CV_AA, 0);
    }
}

/**
 * Extracts all necessary infomration about car (currCar) from one annotaion 
 * line (aData).
 * @param aData
 * @param currCar
 * @return 
 */
bool getCarData(stringstream& aData, Car& currCar) {
    int number = 0;
    int cnt = 0;
    while (aData >> number) {
        // one comma separated number extracted in number variable
        switch (cnt) {
            case 0: // first is ID
                currCar.ID = number;
                break;

            case 1: // second is frame number
                currCar.frame = number;
                break;

            default: // the rest are BB points 
                if ((cnt - 2) % 2 == 0) {
                    currCar.P[(cnt - 2) / 2].x = number;
                } else {
                    currCar.P[(cnt - 2) / 2].y = number;
                } // some magic happened to save the BB points
                break;
        }

        // finding next comma
        if (aData.peek() == ',') {
            aData.ignore();
        }
        cnt++;
    }
    if (cnt < ANNOT_LINE_PARTS)
        return false; // not all required numbers extracted
    else
        return true;
}

/**
 * Save car part images (combined and representative).
 * @param currCarVec
 * @param argv
 */
void storeCarImages(vector<Car>& currCarVec, char ** argv) {
    Car combCar;
    stringstream oFile_name;
    for (Car tmpCar : currCarVec) {
        tmpCar.combine();
        // saving car combined image
        oFile_name.str("");
        oFile_name.clear();
        oFile_name << "./" << argv[1] << "/" << tmpCar.ID << "_" << tmpCar.frame << "_combined" << ".png"; //todo change
        cout << "Saving combined image to " << oFile_name.str() << '\n';
        imwrite(oFile_name.str(), tmpCar.combined);
    }

    // saving car representative image (ID_FRAME_representative.png)
    oFile_name.str("");
    oFile_name.clear();
    if (currCarVec.at(0).P[4].x > currCarVec.at(1).P[4].x) // from camera car direction
        combCar = currCarVec.at(1);
    else // to camera car direction
        combCar = currCarVec.at(currCarVec.size() - 3);
    combCar.combine(); // combining side + front images
    oFile_name << "./" << argv[1] << "/" << combCar.ID << '_' << combCar.frame << "_representative.png";
    imwrite(oFile_name.str(), combCar.combined); // saving representative car image

    // saving squared car image
    oFile_name.str("");
    oFile_name.clear();
    oFile_name << "./" << argv[1] << "/" << combCar.ID << '_' << combCar.frame << "_squared.png";
    /*cap.set(CV_CAP_PROP_POS_FRAMES, combCar.frame);
    cap.read(image);
    Mat(image, Rect(combCar.P[2].x < 0 ? 0 : combCar.P[2].x, combCar.P[2].y < 0 ? 0 : combCar.P[2].y, combCar.P[4].x - combCar.P[2].x, combCar.P[4].y - combCar.P[2].y)).copyTo(combCar.squared);
    resize(combCar.squared, combCar.squared, Size(150, 100));
    imwrite(oFile_name.str(), combCar.squared); // saving squared car image*/
}

/**
 * Prints help message to standard output.
 */
void printHelp(){
    cout << "VEHICLE RE-IDENTIFICATION IN VIDEO - EXTRACTOR\n\n"
            << "Author: Dominik Zapletal (login: xzaple34) (c) 2015\n"
            << "Info:   Bachelor's thesis, subject IBP - VUT FIT, Brno\n"
            << "Usage: ./extractor [VIDEO FILE]\n"
            << "(without file extension .mov/.txt)\n";
}

/**
 * Extracts all car parts from video using annotation files.
 * @function main
 */
int main(int argc, char** argv) {
    
    if(argc != 2){
        cout << "Argument syntactic error!\n";
        printHelp();
        return -1;
    }
    
    // variables
    Mat image;
    ifstream annotations;

    // filename variables
    stringstream aFile_name;
    stringstream vFile_name;

    // annotation read variables
    stringstream aData;
    string line;

    // car variables
    Car prevCar;
    Car currCar;
    Car tmpCar;
    vector<Car> currCarVec;

    // final car parts sizes
    Size sideSize = Size(208, 104);
    Size frontSize = Size(104, 104);

    // file names
    aFile_name << argv[1] << "_annotations.txt";
    vFile_name << argv[1] << ".mov";

    // opening annotation file
    annotations.open(aFile_name.str());
    if (!annotations.is_open()) {
        cout << "Cannot open the annotations file for video " << argv[1] << endl;
        printHelp();
        return -1;
    }

    // opening video file
    VideoCapture cap(vFile_name.str());
    if (!cap.isOpened()) {
        cout << "Cannot open the video file " << endl;
        printHelp();
        return -1;
    }

    // starting extraction
    mkdir(argv[1], 0777);
    while (!annotations.eof()) {
        // clearing variables
        line = "";
        aData.str("");
        aData.clear();

        // processing one line in annotation file
        getline(annotations, line);
        aData.str(line);
        cout << "PROCESSING ANNOTATION LINE " << line << '\n'; // processing annotation line information

        // getting data from the line
        if (!getCarData(aData, currCar)) {
            continue;
        }

        cout << cap.get(CV_CAP_PROP_FPS) << '\n'; // fps information print

        if (argv[1][1] == 'A') // frame shift to fit BB - frame rate problem
            currCar.frame--;

        // getting next frame from video file
        cap.set(CV_CAP_PROP_POS_FRAMES, currCar.frame);
        cap.read(image);

        if (currCar.ID != prevCar.ID) { // new car recognised => save previous
            if (currCarVec.size() > MINIMUM_CAR_REC) { // valid car annotation/detection
                storeCarImages(currCarVec, argv);
            }
            currCarVec.clear();
            prevCar = currCar;
        }

        // warping side and front part of a 3D BB into a plane
        currCar.side = getRectangledArea(image, sideSize, currCar.P[2], currCar.P[1], currCar.P[5],
                currCar.P[6]);
        currCar.front = getRectangledArea(image, frontSize, currCar.P[1], currCar.P[0], currCar.P[4],
                currCar.P[5]);

        // storing car occurrences into a vector
        currCarVec.push_back(currCar);
    }

    if (currCarVec.size() > MINIMUM_CAR_REC) { // valid car annotation/detection
        storeCarImages(currCarVec, argv);
    }
    currCarVec.clear();

    // releasing sources
    cap.release();
    annotations.close();
    return 0;
}