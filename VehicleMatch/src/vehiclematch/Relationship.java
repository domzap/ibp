/**
 * Thesis name: Vehicle Re-identification in Video 
 * File name: Relationship.java
 * Written by Dominik Zapletal <xzaple34@stud.fit.vutbr.cz>, May 2015
 */

package vehiclematch;

/**
 * Represents a relationship between images.
 */
public class Relationship {

    public SquarePane S;
    public boolean Type;

    public Relationship(SquarePane s, boolean  type) {
        S = s;
        Type = type;
    }
}
