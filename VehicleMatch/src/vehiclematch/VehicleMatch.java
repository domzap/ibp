/**
 * Thesis name: Vehicle Re-identification in Video 
 * File name: VehiclMatch.java
 * Description: This program allows the user to quickly pair images from 
 *              different folders. Used for vehicle pairing.
 * Written by Dominik Zapletal <xzaple34@stud.fit.vutbr.cz>, May 2015
 */
package vehiclematch;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.*;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.BoxLayout;

/**
 * Main JFrame class.
 */
public class VehicleMatch extends JFrame {

    // class variables
    private static VehicleMatch m;
    private JFrame frame;
    public HashMap<String, SquarePane> AHtab;
    public HashMap<String, SquarePane> BHtab;
    JPanel picturesA;
    JPanel picturesB;
    boolean controlPressed;
    JScrollPane aScroll;
    JScrollPane bScroll;
    MyGlassPane G;
    String APath;
    String BPath;
    int ASVal;
    int BSVal;

    // states for matching file loading
    public enum STATE {

        START,
        SOURCES,
        FirstSource,
        SecondSource,
        RELATIONS,
        Apicture,
        Relation,
        Bpicture,
        SETTINGS,
        Ascroll,
        AscrollValue,
        Bscroll,
        BscrollValue,
    }

    /**
     * Loads representative pictures stored in Path and adds them to JPanel.
     *
     * @param picP
     * @param Path
     */
    public void addPictures2Pane(JPanel picP, String Path) {
        HashMap<String, SquarePane> currTab;
        SquarePane tmpSquare;
        if (picP.getName().equals("A")) {
            APath = Path;
            currTab = AHtab;
        } else {
            BPath = Path;
            currTab = BHtab;
        }
        final File folder = new File(Path);
        File[] files = folder.listFiles();
        Arrays.sort(files, new Comparator<File>() {
            @Override
            public int compare(File s1, File s2) {
                return Integer.valueOf(s1.getName().substring(0, s1.getName().indexOf("_"))).compareTo(Integer.valueOf(s2.getName().substring(0, s2.getName().indexOf("_"))));
            }
        });
        for (final File fileEntry : files) {
            try {
                System.out.println(fileEntry.getAbsolutePath() + "\n");
                if (fileEntry.getAbsolutePath().endsWith(".PNG") || fileEntry.getAbsolutePath().endsWith("representative.png")) {
                    tmpSquare = new SquarePane(fileEntry.getAbsolutePath(), picP.getWidth() / 2, picP);
                    currTab.put(fileEntry.getAbsolutePath(), tmpSquare);
                    picP.add(tmpSquare);
                }
            } catch (IOException ex) {
                Logger.getLogger(DemoJFileChooser.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Sets up main window components and adding them to the JFrame.
     */
    public void setFrame() {
        //Create and set up the window.
        frame = new JFrame("Vehicle match");
        frame.setLayout(new GridLayout(1, 2));
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        frame.setMinimumSize(new Dimension(800, 600));
        frame.setResizable(true);
        frame.setFocusable(true);
        AHtab = new HashMap<>();
        BHtab = new HashMap<>();
        controlPressed = false;
        picturesA = new JPanel();
        picturesA.setName("A");
        picturesA.setLayout(new BoxLayout(picturesA, BoxLayout.Y_AXIS));
        aScroll = new JScrollPane(picturesA);
        aScroll.getVerticalScrollBar().setUnitIncrement(20);
        JPanel tmp = new JPanel(new BorderLayout());
        JScrollBar sb = aScroll.getVerticalScrollBar();
        tmp.add(aScroll, BorderLayout.CENTER);
        tmp.add(sb, BorderLayout.WEST);
        frame.add(tmp);
        aScroll.getVerticalScrollBar().addAdjustmentListener(new AdjustmentListener() {

            @Override
            public void adjustmentValueChanged(AdjustmentEvent e) {
                frame.getGlassPane().repaint();
            }

        });
        G = new MyGlassPane(picturesA);
        frame.setGlassPane(G);
        G.setVisible(true);
        picturesB = new JPanel();
        picturesB.setName("B");
        picturesB.setLayout(new BoxLayout(picturesB, BoxLayout.Y_AXIS));
        bScroll = new JScrollPane(picturesB);
        bScroll.getVerticalScrollBar().setUnitIncrement(20);
        bScroll.getVerticalScrollBar().addAdjustmentListener(new AdjustmentListener() {
            int PrevValue = 0;
            int direct = 1;

            @Override
            public void adjustmentValueChanged(AdjustmentEvent e) {
                if (PrevValue < bScroll.getVerticalScrollBar().getValue()) {
                    direct = 1;
                } else {
                    direct = -1;
                }
                if (controlPressed) {
                    aScroll.getVerticalScrollBar().setValue(aScroll.getVerticalScrollBar().getValue() + aScroll.getVerticalScrollBar().getUnitIncrement() * direct);
                }
                PrevValue = bScroll.getVerticalScrollBar().getValue();
                frame.getGlassPane().repaint();
            }

        });
        frame.add(bScroll);

        frame.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_CONTROL) {
                    controlPressed = true;
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_CONTROL) {
                    controlPressed = false;
                }
            }
        });
    }

    /**
     * Loads configuration file.
     * @param Path
     * @return 
     */
    public boolean openCurrentCFG(String Path) {
        BufferedReader br;
        SquarePane tmpA = null;
        ASVal = 0;
        BSVal = 0;
        boolean tmpRel = false;
        try {
            STATE currentState = STATE.START;
            br = new BufferedReader(new FileReader(Path));
            String line;
            while ((line = br.readLine()) != null) {
                System.out.println(line + " " + currentState);
                switch (currentState) {
                    case START:
                        if (line.equals("[SOURCES]")) {
                            currentState = STATE.FirstSource;
                        } else {
                            return false;
                        }
                        break;

                    case FirstSource:
                        addPictures2Pane(picturesA, line);
                        currentState = STATE.SecondSource;
                        break;

                    case SecondSource:
                        addPictures2Pane(picturesB, line);
                        frame.pack();
                        currentState = STATE.RELATIONS;
                        break;

                    case RELATIONS:
                        if (line.equals("[RELATIONS]")) {
                            currentState = STATE.Apicture;
                        } else {
                            return false;
                        }
                        break;

                    case Apicture:
                        if (line.equals("[SETTINGS]")) {
                            currentState = STATE.SETTINGS;
                        } else {
                            tmpA = AHtab.get(line);
                            if (tmpA == null) {
                                return false;
                            }
                            currentState = STATE.Relation;
                        }
                        break;

                    case Relation:
                        if (line.equals("RelatedTo")) {
                            tmpRel = true;
                        } else if (line.equals("NotRelatedTo")) {
                            tmpRel = false;
                        } else {
                            return false;
                        }
                        currentState = STATE.Bpicture;
                        break;

                    case Bpicture:
                        G.pushPic(tmpA, tmpRel);
                        G.pushPic(BHtab.get(line), tmpRel);
                        currentState = STATE.Apicture;
                        break;

                    case SETTINGS:
                        currentState = STATE.Ascroll;
                        break;

                    case Ascroll:
                        ASVal = Integer.valueOf(line);
                        currentState = STATE.Bscroll;
                        break;

                    case Bscroll:
                        currentState = STATE.BscrollValue;
                        break;

                    case BscrollValue:
                        BSVal = Integer.valueOf(line);
                        currentState = STATE.Bscroll;
                        break;
                }
            }
            br.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(VehicleMatch.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(VehicleMatch.class.getName()).log(Level.SEVERE, null, ex);
        }
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                aScroll.getVerticalScrollBar().setValue(ASVal);
            }
        });
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                bScroll.getVerticalScrollBar().setValue(BSVal);
            }
        });
        return true;
    }

    /**
     * Saves new configuration file.
     * @param Path 
     */
    public void saveCurrentCFG(String Path) {
        try (PrintWriter writer = new PrintWriter(Path, "UTF-8")) {
            writer.println("[SOURCES]");
            writer.println(APath);
            writer.println(BPath);
            writer.println("[RELATIONS]");
            for (Map.Entry pairs : G.relations.entrySet()) {
                SquarePane tmpS = (SquarePane) pairs.getValue();
                for (Map.Entry pairs2 : tmpS.Relations.entrySet()) {
                    Relationship tmpRel = (Relationship) pairs2.getValue();
                    writer.println(tmpS.ID);
                    if (!tmpRel.Type) {
                        writer.println("NotRelatedTo");
                    } else {
                        writer.println("RelatedTo");
                    }
                    writer.println(tmpRel.S.ID);
                }
            }
            writer.println("[SETTINGS]");
            writer.println("ASCROLL");
            writer.println(aScroll.getVerticalScrollBar().getValue());
            writer.println("BSCROLL");
            writer.println(bScroll.getVerticalScrollBar().getValue());
            writer.close();
        } catch (FileNotFoundException | UnsupportedEncodingException ex) {
            Logger.getLogger(VehicleMatch.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Fills JFrame with necessary components.
     */
    public void fill() {
        JMenuBar menuBar;
        JMenu menu;
        JMenuItem menuItem;
        menuBar = new JMenuBar();
        menu = new JMenu("File");
        menuBar.add(menu);
        menuItem = new JMenuItem("Folder A");
        DemoJFileChooser Chooser = new DemoJFileChooser(picturesA, "/home/lenovo/detection", this);
        menuItem.addActionListener(Chooser);
        menu.add(menuItem);
        menuItem = new JMenuItem("Folder B");
        Chooser = new DemoJFileChooser(picturesB, "/home/lenovo/Obrázky", this);
        menuItem.addActionListener(Chooser);
        menu.add(menuItem);
        menuItem = new JMenuItem("Save current matching file");
        final JFileChooser fileChooser = new JFileChooser();
        fileChooser.setCurrentDirectory(new java.io.File("."));
        fileChooser.setDialogTitle("Save current matching file");
        menuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (fileChooser.showSaveDialog(fileChooser) == JFileChooser.APPROVE_OPTION) {
                    saveCurrentCFG("" + fileChooser.getSelectedFile());
                }
            }
        });
        menu.add(menuItem);
        menuItem = new JMenuItem("Load matching file");
        final JFileChooser fileChooser2 = new JFileChooser();
        fileChooser2.setCurrentDirectory(new java.io.File("."));
        fileChooser2.setDialogTitle("Load matching file");
        menuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (fileChooser2.showOpenDialog(fileChooser) == JFileChooser.APPROVE_OPTION) {
                    deleteAllContent();
                    openCurrentCFG("" + fileChooser2.getSelectedFile());
                }
            }
        });
        menu.add(menuItem);

        frame.setJMenuBar(menuBar);
        frame.validate();
        frame.repaint();
        frame.pack();
        frame.setVisible(true);
    }

    /**
     * Deletes content of a JFrame components. Used for configuration file 
     * reloading.
     */
    public void deleteAllContent() {
        picturesA.removeAll();
        picturesB.removeAll();
        AHtab.clear();
        BHtab.clear();
        G.relations.clear();
        System.gc();
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        g.drawString("Hello", this.getX() + 100, this.getY() + 50);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                m = new VehicleMatch();
                m.setFrame();
                m.fill();
            }
        });
    }

}
