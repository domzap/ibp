/**
 * Thesis name: Vehicle Re-identification in Video 
 * File name: SquarePane.java
 * Written by Dominik Zapletal <xzaple34@stud.fit.vutbr.cz>, May 2015
 */

package vehiclematch;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javax.imageio.ImageIO;
import javax.imageio.ImageReadParam;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import javax.swing.*;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 * This class represents one image.
 */
public class SquarePane extends JPanel {

    SquarePane me;
    String ID;
    private final BufferedImage img;
    private final int width;
    private final int height;
    boolean selected;
    boolean enemy;
    JPanel parentPane;
    JFrame mainFrame;
    MyGlassPane glassP;
    HashMap<String, Relationship> Relations;
    Point center;

    /**
     * Returns true if selected.
     * @return 
     */
    public boolean isSelected() {
        return selected;
    }

    /**
     * Sets selection.
     * @param sel 
     */
    public void setSelection(boolean sel) {
        selected = sel;
    }

    /**
     * Returns to which panel (left or right) the image belongs.
     * @return 
     */
    int getOrigin() {
        if (parentPane.getName().equals("A")) {
            return 0;
        } else {
            return 1;
        }
    }

    /**
     * Adds new relation to the image (it's double).
     * @param f
     * @param type 
     */
    public void addRel(SquarePane f, boolean type) {
        Relations.put(f.ID, new Relationship(f, type));
    }
    
    /**
     * Removes all relations from the image.
     */
    private void removeRelations() {
        Iterator it = Relations.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pairs = (Map.Entry) it.next();
            Relationship tmpRel = (Relationship) pairs.getValue();
            tmpRel.S.Relations.remove(me.ID);
            //Relations.remove(tmpRel.S.ID);
            it.remove(); // avoids a ConcurrentModificationException
        }
        glassP.repaint();
    }

    /**
     * Returns existance of any relation.
     * @return 
     */
    public boolean haveRelations() {
        return !Relations.isEmpty();
    }

    /**
     * Returns coordinates of a point to be drawn.
     * @return 
     */
    Point getPoint2Draw() {
        int x = this.getLocationOnScreen().x - mainFrame.getGlassPane().getLocationOnScreen().x;
        int y = this.getLocationOnScreen().y - mainFrame.getGlassPane().getLocationOnScreen().y;
        return new Point(center.x + x, center.y + y);
    }

    /**
     * Returns re-scaled image.
     * @param ratio
     * @return
     * @throws IOException 
     */
    private BufferedImage scale(double ratio) throws IOException {
        BufferedImage source = ImageIO.read(new File(ID));
        int w = (int) (source.getWidth() * ratio);
        int h = (int) (source.getHeight() * ratio);
        BufferedImage bi = getCompatibleImage(w, h);
        Graphics2D g2d = bi.createGraphics();
        double xScale = (double) w / source.getWidth();
        double yScale = (double) h / source.getHeight();
        AffineTransform at = AffineTransform.getScaleInstance(xScale, yScale);
        g2d.drawRenderedImage(source, at);
        g2d.dispose();
        return bi;
    }

    /**
     * Returns re-sized image.
     * @param w
     * @param h
     * @return 
     */
    private BufferedImage getCompatibleImage(int w, int h) {
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice gd = ge.getDefaultScreenDevice();
        GraphicsConfiguration gc = gd.getDefaultConfiguration();
        BufferedImage image = gc.createCompatibleImage(w, h);
        return image;
    }

    /**
     * SquarePane initialization. Adding necessary components.
     * @param imagePath
     * @param w
     * @param parent
     * @throws IOException 
     */
    public SquarePane(String imagePath, int w, JPanel parent) throws IOException {
        setLayout(new BorderLayout());
        me = this;
        Relations = new HashMap<>();
        selected = false;
        enemy = false;
        parentPane = parent;
        mainFrame = (JFrame) SwingUtilities.getWindowAncestor(parentPane);
        glassP = (MyGlassPane) mainFrame.getGlassPane();
        width = w;
        ID = imagePath;
        
        FileInputStream fin = new FileInputStream(ID);
        ImageInputStream iis = ImageIO.createImageInputStream(fin);
        Iterator iter = ImageIO.getImageReaders(iis);
        /*if (!iter.hasNext()) {
            return;
        }*/
        ImageReader reader = (ImageReader) iter.next();
        ImageReadParam params = reader.getDefaultReadParam();
        reader.setInput(iis, true, true);
        params.setSourceSubsampling(1, 1, 0, 0);

        img = reader.read(0, params);
        height = (int) (w * ((float) img.getHeight(null) / img.getWidth(null)));
        center = new Point(this.width / 2, this.height / 2);
        //img = img.getScaledInstance(width, height, );
        System.out.println(width + " x " + height + "\n");
        addKeyListener(new KeyListener() {

            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == e.VK_DELETE) {
                    removeRelations();
                    me.setSelection(false);
                    glassP.clearSelection();
                    mainFrame.requestFocusInWindow();
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {
            }
        });
        addMouseListener(new MouseListener() {

            @Override
            public void mouseClicked(MouseEvent e) {
                boolean tmpSelection = selected;
                Component[] components = parentPane.getComponents();
                for (int i = 0; i < components.length; ++i) {
                    if ((components[i] instanceof SquarePane)) {
                        SquarePane subContainer = (SquarePane) components[i];
                        subContainer.setSelection(false);
                    }
                }
                selected = tmpSelection;
                selected = !selected;
                if (selected) {
                    me.requestFocusInWindow();
                    if (e.getButton() == 3) {
                        glassP.pushPic(me, false);
                    } else {
                        glassP.pushPic(me, true);
                    }
                } else {
                    glassP.clearSelection();
                    mainFrame.requestFocusInWindow();
                }
                glassP.repaint();
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });
    }

    @Override
    public Dimension getMinimumSize() {
        return getPreferredSize();
    }

    @Override
    public Dimension getMaximumSize() {
        return getPreferredSize();
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(width, height);
    }

    /**
     * Draws image in the middle of the SquarePane.
     * @param page
     */
    @Override
    public void paintComponent(Graphics page) {
        super.paintComponent(page);
        int x = (getWidth() - img.getWidth(null)) / 2;
        int y = (getHeight() - img.getHeight(null)) / 2;
        Graphics2D g2 = (Graphics2D) page;
        g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.drawImage(img, 0, 0,width,height, null);
        if (selected) {
            g2.setColor(new Color(0, 255, 0, 100));
            g2.fillRect(0, 0, width, height);
            g2.setColor(Color.yellow);
            g2.fillOval(center.x - 10, center.y - 10, 20, 20);
        } else if (me.haveRelations()) {
            g2.setColor(new Color(0, 0, 255, 100));
            g2.fillRect(0, 0, width, height);
            g2.setColor(Color.yellow);
            g2.fillOval(center.x - 10, center.y - 10, 20, 20);
        }
    }
}
