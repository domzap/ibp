/**
 * Thesis name: Vehicle Re-identification in Video 
 * File name: MyGlassPane.java
 * Written by Dominik Zapletal <xzaple34@stud.fit.vutbr.cz>, May 2015
 */

package vehiclematch;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JComponent;
import javax.swing.JPanel;

/**
 * Class MyGlassPane represents top layer of a application's JFrame.
 */
public class MyGlassPane extends JComponent {

    // class variables
    JPanel pictures;
    int shiftx;
    int shifty;
    SquarePane aPic;
    SquarePane bPic;
    public HashMap<String, SquarePane> relations;

    /**
     * MyGlassPane constructor.
     * @param p 
     */
    public MyGlassPane(JPanel p) {
        pictures = p;
        aPic = null;
        bPic = null;
        relations = new HashMap<>();

    }

    /**
     * Adds input SquarePane origin.
     * @param p 
     */
    private void choose(SquarePane p) {
        if (p.getOrigin() == 0) {
            aPic = p;
        } else {
            bPic = p;
        }
    }

    /**
     * Different pictures are selected or not.
     * @return 
     */
    private boolean differentPic() {
        return aPic != null && bPic != null;
    }
    
    /**
     * Clears selection.
     */
    public void clearSelection(){
        aPic = null;
        bPic = null;
    }

    /**
     * Creates both-sided picture relation.
     * @param pic
     * @param type 
     */
    public void pushPic(SquarePane pic, boolean type) {
        choose(pic);
        if (differentPic()) {
            aPic.addRel(bPic, type);
            bPic.addRel(aPic, type);
            relations.put(aPic.ID, aPic);
            aPic.setSelection(false);
            bPic.setSelection(false);
            bPic.mainFrame.requestFocusInWindow();
            aPic = null;
            bPic = null;
        }
    }

    /**
     * Draws all visible relations between pictures.
     * @param g 
     */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(
                RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setStroke(new BasicStroke(3));
        for (Map.Entry pairs : relations.entrySet()) {
            SquarePane tmpS = (SquarePane) pairs.getValue();
            for (Map.Entry pairs2 : tmpS.Relations.entrySet()) {
                Relationship tmpRel = (Relationship) pairs2.getValue();
                if(!tmpRel.Type)
                    g2.setColor(Color.RED);
                else
                    g2.setColor(Color.GREEN);
                g2.drawLine(tmpS.getPoint2Draw().x, tmpS.getPoint2Draw().y,
                        tmpRel.S.getPoint2Draw().x, tmpRel.S.getPoint2Draw().y);
            }
        }

    }
}
