/**
 * Thesis name: Vehicle Re-identification in Video 
 * File name: DemoJFileChooser.java
 * Written by Dominik Zapletal <xzaple34@stud.fit.vutbr.cz>, May 2015
 */

package vehiclematch;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 * Represents a modified file chooser. Current folder is selected.
 */
public class DemoJFileChooser extends JPanel
        implements ActionListener {

    JFileChooser chooser;
    JPanel pictureBoxes;
    VehicleMatch vm;
    JFrame mainFrame;

    public DemoJFileChooser(JPanel pictures, String P, VehicleMatch VM) {
        pictureBoxes = pictures;
        vm = VM;
        mainFrame = (JFrame) SwingUtilities.getWindowAncestor(pictureBoxes);
        //vm.addPictures2Pane(pictures, P);
        //mainFrame.pack();
    }

    /**
     * Current directory is selected.
     * @param e 
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        pictureBoxes.removeAll();
        vm.G.relations.clear();
        System.gc();
        chooser = new JFileChooser();
        chooser.setCurrentDirectory(new java.io.File("."));
        chooser.setDialogTitle("Choose photo directory");
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        //
        // disable the "All files" option.
        //
        chooser.setAcceptAllFileFilterUsed(false);
        //    
        if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            System.out.println("getCurrentDirectory(): "
                    + chooser.getCurrentDirectory());
            System.out.println("getSelectedFile() : "
                    + chooser.getSelectedFile());
            vm.addPictures2Pane(pictureBoxes, "" + chooser.getSelectedFile());
        } else {
            System.out.println("No Selection ");
        }
        mainFrame.pack();
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(200, 200);
    }
}
