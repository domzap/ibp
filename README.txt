================================================================================

REIDENTIFIKACE AUTOMOBILŮ VE VIDEU
Autor: Dominik Zapletal
E-mail: xzaple34@stud.fit.vutbr.cz
Vedoucí práce: doc. Ing. Adam Herout, Ph.D.
VYSOKÉ UČENÍ TECHNICKÉ, FAKULTA INFORMAČNÍCH TECHNOLOGIÍ, BRNO, 2015

================================================================================

V práci byla použita knihovna OpenCV pro práci s obrazem. Dále byla použita 
knihovna LIBLINEAR pro trénování lineárního SVM klasifikátoru a následnou 
regresi. Ve zdrojových souborech je použito pár funkcí, jejiž autorem nejsem já.
V takových případech je u dané funkce odkaz na zdroj, odkud byla stažena.

DŮLEŽITÉ: Pro spuštění všech programů mající jako parametr soubor z GUI aplikace
(train, reidentification) pro vytváření párů stejných automobilů je nutné buď
v těchto souborech změnit absolutní cesty k obrázkům nebo vytvořit aplikací
soubory nové na konkrétním počítači.

================================================================================

Popis obsahu této složky:

--------------------------------------------------------------------------------

WebpageDatabase
	POPIS:
	- Složka obsahuje SQL skripty pro inicializaci a aktualizaci hodnot v 
	  databázi na webové stránce, která byla použita pro vyhodnocení 
	  reidentifikačního algoritmu. Dále je zde databáze obrázků automobilů
	  (složka img_database).

--------------------------------------------------------------------------------

VehicleMatch
	POPIS:
	- Složka obsahuje zdrojovou i binární podobu Java programu (GUI), které
	  slouží pro párování obrázků automobilů a vygenerování souboru o 
	  vytvořených vazbách. Jsou zde také ukázkové výstupní soubory shot_1-5.
	  Tyto soubory slouží jako vstup pro trénování (program train) lineárního
	  klasifikátoru (vytvoření modelu) a pro experimentování s různými parametry
	  re-identifikace (program reidentification).
	- Pro přeložení a spuštění programu je nutné mít nainstalován Java RE 6 
	  a vyšší.

	PŘELOŽENÍ:
	- ve složce ./VehicleMatch příkazem:
	  ant

	SPUŠTĚNÍ:
	- ve složce  ./VehicleMatch/dist příkazem:
	  java -jar "VehicleMatch.jar" 

	OVLÁDÁNÍ:
	- Po spuštění lze v levém horním menu nahrát existující konfigurační soubor
	  ("Load matching file") nebo načíst obrázky ze dvou složek ("Folder A", 
	  "Folder B") a vytvářet jednotlivé páry automobilů. Kliknutím na obrázek
	  dojde k jeho označení (zmodrá). Po kliknutí na odpovídající obrázek v 
	  druhé množině dojde k vytvoření vazby. Vazba lze zrušit opětovných 
	  kliknutím na jeden z obrázků ve vazbě a zmáčknutím klávesy "Delete". Pro
	  synchronní posun obou sloupců obrázků zároveň je nutné najet myšší na 
	  pravý sloupec a pak následným otáčením kolečkem myši při stisknuté levé
	  klávese "Ctrl" dojde k posunu obou sloupců. Po dokončení vytvoření vazeb
	  je možné konfigurační soubor uložit volbou "Save current matching file"
	  v levém horním menu.

--------------------------------------------------------------------------------

Re-identification
	POPIS:
	- Složka obsahuje zdrojové i binární podoby programů pro trénování (pogram
	  train) lineárního SVM klasifikátoru (použita knihovna LIBLINEAR - zdrojové
	  soubory napsané v jazyce C) a pro experimentování s různými parametry 
	  re-identifikace automobilů (program reidentification). Dále jsou zde 
	  ukázky různých druhů modelů (složka HIST_HOG_models), vygenerované 
	  trénovací sady s použitím různých porovnávacích metod barevných histogramů
	  (složka color_hist_comparison_methods_train_sets) a ukázky vykreslených 
	  barevných histogramů a histogramů orientovaných gradientů (složka 
	  color_hist_and_HOG_plots). Ukázkové modely dosahující nejlepších výsledků
	  jsou uloženy v kořenu složky. 
	- Pro přeložení a spuštění programů je nutné mít nainstalovánou knihovnu
	  OpenCV verze 2.4.9 a vyšší.

	PŘELOŽENÍ:
	- ve složce ./Re-identification příkazem:
	  make
	- Přeloženy budou všechny potřebné zdrojové soubory.
	- smazání objektových souborů je možné provést příkazem:
	  make clean

	SPUŠTĚNÍ train programu:
	- ve složce ./Re-identification příkazem:
	  ./train s následujícími parametry
	-n          output_model_name        :jméno výstupního modelu
	-m          [matching_files ...]     :vstupní soubory z GUI aplikace - jsou 
					      brány jako parametry do konce 
					      příkazové řádky
	-hog                                 :typ modelu (defaultně je nastaven
					      barevný histogram)
	-pos        pos_train_number         :počet pozitivních vzorků pro jeden
					      pár automobilů při trénování 
	-neg        neg_mul_number           :násobek celkového počtu 
					      pozitivních vorků pro trénování 
					      negativních vzorků

	- výstupem programu je model pro výpočet regrese

	SPUŠTĚNÍ reidentification programu:
	- ve složce ./Re-identification příkazem:
	  ./reidentification s následujícími parametry:
	  -hist       [colour_histogram_model] :název modelu pro barevný 
						histogram
	  -hog        [hog_model]              :název modelu pro HOG
	  -m          [matching files ...]     :vstupní soubory z GUI aplikace - 
						jsou brány jako parametry do 
						konce příkazové řádky

	- Výstup programu je procentuální úspěšnost reidentifikace jednotlivých
	  automobilů načtených ze vstupních konfiguračních souborů GUI aplikace 
	  včetně různých dodatečných informací. Načítání je urychleno 
	  serializací příznakových vektorů do složky se zdrojovými obrázky 
	  (binární soubory .hist a .hog). Pro znovuvypočítání příznakových 
	  vektorů je potřeba tyto soubory smazat.

--------------------------------------------------------------------------------

Extractor
	POPIS:
	- Složka obsahuje zdrojovou i binární podobu C++ programu pro extrahování a 
	  rektifikaci automobilů z videa na základě anotačních souborů vytvořených
	  detekčním systémem. Dále jsou zde již extrahované obrázky automobilů z 
	  jednotlivých videí a jeden záběr včetně anotačního souboru pro ukázku.
	- Pro přeložení a spuštění programu je nutné mít nainstalovánou knihovnu
	  OpenCV verze 2.4.9 a vyšší.

	PŘELOŽENÍ:
	- ve složce ./Extractor příkazem:
	  make
	- smazání objektových souborů je možné provést příkazem:
	  make clean

	SPUŠTĚNÍ reidentification programu:
	- ve složce ./Extractor příkazem:
	  ./extractor [VIDEO FILE]
	- Výstupem pogramu jsou obrázky obsahující rektifikované automobily. 
	  Parametr programu musí být název video souboru bez přípony .mov. Název
	  anotačního souboru je odvozen od názvu video souboru. Vygenerované 
	  obrázky jsou pak uloženy ve složce s příslušným názvem video souboru.
