/**
 * Thesis name: Vehicle Re-identification in Video
 * File name: train.cpp
 * Description: This program creates HOG/colour histogram models for regression.
 * Written by Dominik Zapletal <xzaple34@stud.fit.vutbr.cz>, May 2015
 */

/*Libraries*/
#include "train.hpp"
#include <fstream>

/*Two car sets hash maps*/
unordered_map<int, Car*> Amap;
unordered_map<int, Car*> Bmap;

/**
 * Deleting all allocated memory.
 */
void releaseSources() {
    if (!Amap.empty()) {
        for (auto it = Amap.begin(); it != Amap.end(); ++it)
            delete it->second;
    }
    if (!Bmap.empty()) {
        for (auto it = Bmap.begin(); it != Bmap.end(); ++it)
            delete it->second;
    }
}

/**
 * Prints help message to standard output.
 */
void printHelp() {
    cout << "VEHICLE RE-IDENTIFICATION IN VIDEO - DATASET TRAINING\n\n"
            << "Author: Dominik Zapletal (login: xzaple34) (c) 2015\n"
            << "Info:   Bachelor's thesis, subject IBP - VUT FIT, Brno\n"
            << "Usage: ./train\n"
            << "-n          output_model_name        :name for output model\n"
            << "-m          [matching_files ...]     :input matching files from GUI\n"
            << "-hog                                 :type of model - colour histogram by\n"
            << "                                      default\n"
            << "-pos        pos_train_number         :number of random regressions per one\n"
            << "                                      positive pair\n"
            << "-neg        neg_mul_number           :positive train amount multiplication for\n"
            << "                                      negative training\n";
}

string modelName;
bool modelType = false; // HOG - true / colour hist - false
vector<string> matchingFiles;
int positiveRepeat = -1;
int negativeMul = -1;

void parseParams(int argc, char** argv) {
    for (int i = 1; i < argc; i++) {
        if (string("-n").compare(argv[i]) == 0) {
            if ((i + 1) < argc) {
                modelName = string(argv[++i]);
            }
        } else if (string("-hog").compare(argv[i]) == 0) {
            modelType = true;
        } else if (string("-pos").compare(argv[i]) == 0) {
            if ((i + 1) < argc) {
                positiveRepeat = atoi(argv[++i]);
            }
        } else if (string("-neg").compare(argv[i]) == 0) {
            if ((i + 1) < argc) {
                negativeMul = atoi(argv[++i]);
            }
        } else if (string("-m").compare(argv[i]) == 0) {
            for (int x = i + 1; x < argc; x++) {
                matchingFiles.push_back(string(argv[x]));
                i++;
            }
        }
    }
}

/**
 * Trains HOG and colour HIST models.
 * @function main
 */
int main(int argc, char** argv) {

    // variables
    Problem problem;
    int carMaxID = -1;
    int carIDfloor = 0;
    int carIDA;
    int carIDB;
    int posCount = 0;
    int negAmount = 0;
    Car * tmpCarA;
    Car * tmpCarB;
    ifstream matchFile;
    string line;

    parseParams(argc, argv);

    // necessary arguments check
    if (modelName.size() == 0 || matchingFiles.size() == 0 || positiveRepeat == -1 || negativeMul == -1) {
        cout << "Argument syntactic error!\n";
        printHelp();
        return -1;
    }

    Combined::training = true; // all features has to be recomputed

    // loading A-B camera cars from matching files
    for (string tmpMatchFile : matchingFiles) {
        cout << "*** Opening matching file " << tmpMatchFile << ". ***\n";
        // opening matching file (positive car pairs)
        matchFile.open(tmpMatchFile);
        if (!matchFile.is_open()) {
            cout << "Cannot open matching file!" << tmpMatchFile << endl;
            return -1;
        }

        // adding A-B cars to hash maps
        cout << "*** Matching file " << tmpMatchFile << " opened. ***\n";
        cout << "*** Loading cars. ***\n";
        fillHashs(Amap, Bmap, matchFile, carMaxID, carIDfloor);
        cout << "*** All cars loaded. ***\n";
        cout << "*** Processing relations. ***\n";

        getline(matchFile, line); // skipping inf. line "[RELATIONS]"

        // positive data training
        while (!matchFile.eof()) {
            getline(matchFile, line); // 1st relation
            if (line.compare("[SETTINGS]") == 0)
                break;
            // data extraction from line
            carIDA = atoi(line.substr(line.rfind("/") + 1, line.substr(line.rfind("/") + 1, line.size() - 1).find("_")).c_str()) + carIDfloor;
            getline(matchFile, line); // "RelatedTo" skipping

            getline(matchFile, line); // 2nd relation
            carIDB = atoi(line.substr(line.rfind("/") + 1, line.substr(line.rfind("/") + 1, line.size() - 1).find("_")).c_str()) + carIDfloor;

            // obtaining pointers to already loaded cars 
            findAndReturn(&Amap, carIDA, &tmpCarA);
            findAndReturn(&Bmap, carIDB, &tmpCarB);
            tmpCarA->myTwin = carIDB;
            tmpCarB->myTwin = carIDA;
            for (int u = 0; u < positiveRepeat; u++) {
                tmpCarA->compareVecWith(tmpCarB, problem, modelType, 1.0); // adding labels to the problem for training
                posCount++;
            }
        }
        carIDfloor += carMaxID; // shifting ID floor in order to load more matching files
        matchFile.close();
    }

    // negative data training
    negAmount = posCount * negativeMul;
    for (int y = 0; y < negAmount; y++) {
        while (!findAndReturn(&Amap, myRandom(0, carIDfloor), &tmpCarA));
        while (!findAndReturn(&Bmap, myRandom(0, carIDfloor), &tmpCarB));
        if (tmpCarB->ID == tmpCarA->myTwin) {
            y--;
            continue;
        }
        tmpCarA->compareVecWith(tmpCarB, problem, modelType, -1.0);
    }

    problem.finalize(); // training problem finalization

    // training process
    struct parameter param;
    struct model* model_;

    // model settings for regression
    param.solver_type = L2R_L2LOSS_SVR;
    param.C = 1;
    param.eps = 0.001;
    param.p = 0.1;
    param.nr_weight = 0;
    param.weight_label = NULL;
    param.weight = NULL;
    const char *error_msg;
    error_msg = check_parameter(&problem.prob, &param);
    if (error_msg) {
        fprintf(stderr, "ERROR: %s\n", error_msg);
        releaseSources();
        exit(1);
    }

    model_ = train(&problem.prob, &param); // start training

    // saving model
    if (save_model(modelName.c_str(), model_)) {
        fprintf(stderr, "can't save model to file \n");
        exit(1);
    }

    // releasing sources
    free_and_destroy_model(&model_);
    destroy_param(&param);
    releaseSources();
    cout << "*** DONE ***\n";
    return 0;
}
