/**
 * Thesis name: Vehicle Re-identification in Video
 * File name: combined.cpp
 * Description: Here are implemented Combined class methods.
 * Written by Dominik Zapletal <xzaple34@stud.fit.vutbr.cz>, May 2015
 */

/*Libraries*/
#include "car.hpp"
#include <numeric>
#include <iostream>
#include <ctime>
#include <unistd.h>
#include <sys/time.h>
#include <fstream>
#include "VRlib.hpp"

bool Combined::training = false;

/**
 * Releases sources.
 */
void Combined::releaseComb() {
    combined.release();
}

/**
 * Loads and stores combined image from input path.
 * @param s
 */
void Combined::pushCombined(string path) {
    path2im = path; // path to combined image
    string subPath = path.substr(0, path.size() - 5);
    // check if binary file is already computed (for faster load)
    if (training || (Car::histValsNum == -1 || Car::hogValsNum == -1) || !exists(string(subPath + ".hist")) || !exists(string(subPath + ".hog"))) {
        combined = imread(path, 1); // loading combined image

        // computing feature vectors and getting it's size
        computeHist(6, 2, 16, 0.5);
        computeHog();
        Car::histValsNum = histVec.size();
        Car::hogValsNum = hogVec.size();

        // saving binary files - serialization
        if (!training) {
            ofstream data_file;
            if (!exists(string(subPath + ".hist"))) {
                data_file.open(string(subPath + ".hist"), ios::out | ios::binary);
                data_file.write(reinterpret_cast<char*> (&histVec[0]), histVec.size() * sizeof (float));
                data_file.close();
            }
            if (!exists(string(subPath + ".hog"))) {
                data_file.open(string(subPath + ".hog"), ios::out | ios::binary);
                data_file.write(reinterpret_cast<char*> (&hogVec[0]), hogVec.size() * sizeof (float));
                data_file.close();
            }
        }
        combined.release(); // releasing combined image
    } else {
        // loading existing serialized binary file of stored feature values
        ifstream data_file;
        data_file.open(string(subPath + ".hist"), ios::in | ios::binary);
        histVec.resize(Car::histValsNum);
        data_file.read(reinterpret_cast<char*> (&histVec[0]), Car::histValsNum * sizeof (float));
        data_file.close();
        data_file.open(string(subPath + ".hog"), ios::in | ios::binary);
        hogVec.resize(Car::hogValsNum);
        data_file.read(reinterpret_cast<char*> (&hogVec[0]), Car::hogValsNum * sizeof (float));
        data_file.close();
    }

    // computing mean feature vectors from loaded binary files
    if (belong2car->meanHistVec.size() == 0) {
        belong2car->meanHistVec.insert(belong2car->meanHistVec.end(), histVec.begin(), histVec.end());
    } else {
        for (int i = 0; i < (int) belong2car->meanHistVec.size(); i++) {
            belong2car->meanHistVec[i] += histVec[i];
            belong2car->meanHistVec[i] /= 2;
        }
    }
    if (belong2car->meanHogVec.size() == 0) {
        belong2car->meanHogVec.insert(belong2car->meanHogVec.end(), hogVec.begin(), hogVec.end());
    } else {
        for (int i = 0; i < (int) belong2car->meanHogVec.size(); i++) {

            belong2car->meanHogVec[i] += hogVec[i];
            belong2car->meanHogVec[i] /= 2;
        }
    }
    //sharpness = varianceOfLaplacian(combined);
}

/**
 * Computes histogram values for whole combined image and stores them in
 * in vector<float>.
 * @param divX - combined image x division
 * @param divY - combined image y division
 * @param bins - number of bins for one cell histogram
 * @param overlap - cell overlap amount (1.0 - no overlap) 
 */
void Combined::computeHist(int divX, int divY, int bins, double overlap) {
    int Xgrid = (divX - 1)*(1 / overlap) + 1;
    int Ygrid = (divY - 1)*(1 / overlap) + 1;
    histVec.reserve(Xgrid * Ygrid * 3 * bins);
    Size smallSize(combined.cols / divX, combined.rows / divY);
    for (int y = 0; (y + smallSize.height - 1) < combined.rows; y
            += (int) (smallSize.height * overlap)) {
        for (int x = 0; (x + smallSize.width - 1) < combined.cols; x
                += (int) (smallSize.width * overlap)) {
            // creating mask
            Rect rect = Rect(x, y, smallSize.width, smallSize.height);
            // extracting one cell
            Mat cell = Mat(combined, rect);
            // compute histogram (BGR) values for the cell
            pushHistValues(cell, histVec, bins);
        }
    }
}

/**
 * Computes HOG values for whole combined image and stores them in
 * in vector<float>.
 */
void Combined::computeHog() {
    Mat grayImg;
    HOGDescriptor hog;
    hog.winSize = reSize;
    hog.blockSize = Size(26, 26);
    hog.blockStride = Size(13, 13);
    hog.cellSize = Size(26, 26);
    vector<Point>locs;
    cvtColor(combined, grayImg, CV_RGB2GRAY);
    hog.compute(grayImg, hogVec, Size(0, 0), Size(0, 0), locs);
}

/**
 * Computes and inserts histogram values into v vector.
 * (http://docs.opencv.org/doc/tutorials/imgproc/histograms/histogram_calculation/histogram_calculation.html)
 * Slightly modified.
 * @param src - cell of the combined image
 * @param v - i/o vector
 * @param bins - number of histogram bins
 */
void Combined::pushHistValues(Mat& src, vector<float>& v, int bins) {
    // Separate the image in 3 places ( B, G and R )

    vector<Mat> bgr_planes;
    split(src, bgr_planes);

    // Establish the number of bins
    int histSize = bins;

    int bins2 = bins * 2;
    int bins3 = bins * 3;

    // Set the ranges ( for B,G,R) )
    float range[] = {0, 256};
    const float* histRange = {range};

    Mat b_hist, g_hist, r_hist;

    // Compute the histograms:
    calcHist(&bgr_planes[0], 1, 0, Mat(), b_hist, 1, &histSize, &histRange,
            true, false);
    calcHist(&bgr_planes[1], 1, 0, Mat(), g_hist, 1, &histSize, &histRange,
            true, false);
    calcHist(&bgr_planes[2], 1, 0, Mat(), r_hist, 1, &histSize, &histRange,
            true, false);

    // Normalize the result to [ 0, 1 ]
    normalize(b_hist, b_hist, 0.0, 1.0, NORM_MINMAX, -1, Mat());
    normalize(g_hist, g_hist, 0.0, 1.0, NORM_MINMAX, -1, Mat());
    normalize(r_hist, r_hist, 0.0, 1.0, NORM_MINMAX, -1, Mat());

    // inserting computed values to one vector in BGR order
    v.insert(v.end(), b_hist.begin<float>(), b_hist.end<float>());
    v.insert(v.end(), g_hist.begin<float>(), g_hist.end<float>());
    v.insert(v.end(), r_hist.begin<float>(), r_hist.end<float>());
}

/**
 * Sets captured frame number.
 * @param f
 */
void Combined::setFrame(int f) {
    frame = f;
}

/**
 * Returns captured frame number.
 * @return 
 */
int Combined::getFrame() {
    return frame;
}

/**
 * Returns combined image reference.
 * @return 
 */
Mat Combined::getCombined() {
    return combined;
}

/**
 * Returns colour histogram feature vector of combined image
 * @return 
 */
vector<float> * Combined::getHistVec() {
    return &histVec;
}

/**
 * Returns HOG feature vector of combined image
 * @return 
 */
vector<float> * Combined::getHogVec() {
    return &hogVec;
}
