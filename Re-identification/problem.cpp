/**
 * Thesis name: Vehicle Re-identification in Video
 * File name: problem.cpp
 * Description: Implementation of Problem class methods.
 * Written by Dominik Zapletal <xzaple34@stud.fit.vutbr.cz>, May 2015
 */

#include "problem.hpp"

using namespace std;

/**
 * Creates new training data input.
 * @param label
 */
void Problem::newTd(double label) {
    y.push_back(label);
    vector<struct feature_node> newFVec;
    x_vec.push_back(newFVec);
}

/**
 * Adds new feature to the training data input.
 * @param index
 * @param value
 */
void Problem::addFeature(int index, double value, vector<feature_node>& x) {
    //tmpMaxIndex = index;
    struct feature_node newFN;
    newFN.index = index;
    newFN.value = value;
    x.push_back(newFN);
    //x_vec[x_vec.size() - 1].push_back(newFN);
}

vector<struct feature_node>* Problem::getLastXvec(){
    return &x_vec[x_vec.size() - 1];
}

/**
 * Closes training data input (last feature -1,?)
 */
void Problem::endTd(int tmpMaxIndex) {
    if (tmpMaxIndex > totalMaxIndex) {
        totalMaxIndex = tmpMaxIndex;
    }
    x.push_back(&x_vec[x_vec.size() - 1][0]);
}

/**
 * Finalizes whole training data structure for liblinear library.  
 */
void Problem::finalize() {
    prob.l = (int) y.size();
    prob.n = totalMaxIndex;
    prob.x = &x[0];
    prob.y = &y[0];
}