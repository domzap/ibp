/**
 * Thesis name: Vehicle Re-identification in Video
 * File name: reidentification.cpp
 * Description: This program serves for experimenting with different reggression
 *              settings and vehicle searching techniques.
 * Written by Dominik Zapletal <xzaple34@stud.fit.vutbr.cz>, May 2015
 */

/*Libraries*/
#include "VRlib.hpp"
#include <fstream>
#include <ctime>
#include <map>
#include <utility> // pair

/*Two car sets hash maps*/
unordered_map<int, Car*> Amap;
unordered_map<int, Car*> Bmap;

/**
 * Deleting all allocated memory.
 */
void releaseSources() {
    if (!Amap.empty()) {
        for (auto it = Amap.begin(); it != Amap.end(); ++it) {
            delete it->second;
        }
    }
    if (!Bmap.empty()) {
        for (auto it = Bmap.begin(); it != Bmap.end(); ++it) {
            delete it->second;
        }
    }
}

/**
 * Prints help message to standard output.
 */
void printHelp() {
    cout << "VEHICLE RE-IDENTIFICATION IN VIDEO - RE-IDENTIFICATION EXPERIMENTING\n\n"
            << "Author: Dominik Zapletal (login: xzaple34) (c) 2015\n"
            << "Info:   Bachelor's thesis, subject IBP - VUT FIT, Brno\n"
            << "Usage: ./reidentification\n"
            << "-hist       [colour_histogram_model] :name of colour histogram model file\n"
            << "-hog        [hog_model]              :name of hog model file\n"
            << "-m          [matching files ...]     :input matching files from GUI\n";
}

string hogModelName;
string histModelName;
vector<string> matchingFiles;

void parseParams(int argc, char** argv) {
    for (int i = 1; i < argc; i++) {
        if (string("-hist").compare(argv[i]) == 0) {
            if ((i + 1) < argc) {
                histModelName = string(argv[++i]);
            }
        } else if (string("-hog").compare(argv[i]) == 0) {
            if ((i + 1) < argc) {
                hogModelName = string(argv[++i]);
            }
        } else if (string("-m").compare(argv[i]) == 0) {
            for (int x = i + 1; x < argc; x++) {
                matchingFiles.push_back(string(argv[x]));
                i++;
            }
        }
    }
}

/**
 * Main function.
 * @param argc
 * @param argv
 * @return 
 */
int main(int argc, char ** argv) {

    //variables 
    string line;
    stringstream tmpString;
    int carIDA;
    int carIDB;
    int foundCount = 0;
    int notFoundCount = 0;
    int falseFound = 0;
    Mat imageA;
    Mat imageB;
    int carMaxID = -1;
    int carIDfloor = 0;
    Car * tmpCarA;
    Car * tmpCarB;
    ifstream matchFile;
    vector<Car*> BcarVec;
    clock_t begin;
    clock_t end;
    Mat outputMat;

    parseParams(argc, argv);

    // necessary arguments check
    if (hogModelName.size() == 0 || histModelName.size() == 0 || matchingFiles.size() == 0) {
        cout << "Argument syntactic error!\n";
        printHelp();
        return -1;
    }

    // loading A-B camera cars from matching files
    begin = clock();
    for (string tmpMatchFile : matchingFiles) {
        cout << "*** Opening matching file " << tmpMatchFile << ". ***\n";
        // opening matching file (positive car pairs)
        matchFile.open(tmpMatchFile);
        if (!matchFile.is_open()) {
            cout << "Cannot open matching file!" << tmpMatchFile << endl;
            return -1;
        }

        // adding A-B cars to hash maps
        cout << "*** Matching file " << tmpMatchFile << " opened. ***\n";
        cout << "*** Loading cars. ***\n";
        fillHashs(Amap, Bmap, matchFile, carMaxID, carIDfloor);

        cout << "*** Processing relations. ***\n";

        getline(matchFile, line); // skipping inf. line "[RELATIONS]"

        // positive data training
        while (!matchFile.eof()) {
            getline(matchFile, line); // 1st relation
            if (line.compare("[SETTINGS]") == 0)
                break;
            // data extraction from line
            carIDA = atoi(line.substr(line.rfind("/") + 1, line.substr(line.rfind("/") + 1, line.size() - 1).find("_")).c_str()) + carIDfloor;
            getline(matchFile, line); // "RelatedTo" skipping

            getline(matchFile, line); // 2nd relation
            carIDB = atoi(line.substr(line.rfind("/") + 1, line.substr(line.rfind("/") + 1, line.size() - 1).find("_")).c_str()) + carIDfloor;

            // obtaining pointers to already loaded cars 
            findAndReturn(&Amap, carIDA, &tmpCarA);
            findAndReturn(&Bmap, carIDB, &tmpCarB);
            BcarVec.push_back(tmpCarB);
            tmpCarA->myTwin = carIDB;
            tmpCarB->myTwin = carIDA;
        }

        carIDfloor += carMaxID;
        cout << "CAR ID FLOOR IS: " << carIDfloor << "\n";
        matchFile.close();
    }
    cout << "*** All cars loaded. ***\n";
    end = clock();
    double elapsed_secs1 = double(end - begin) / CLOCKS_PER_SEC;

    // loading HOG and colour histogram models
    struct model* regressionModelHist_;
    struct model* regressionModelHog_;
    string regressionHistModel = "A_B_A-B_6x2_overlap_shot4-5_x60_x2_16bin_HIST_L2R_L2LOSS_SVR.model";
    string regressionHogModel = "A_B_A-B_12x4_overlap_shot4-5_x60_x2_HOG_L2R_L2LOSS_SVR.model";
    regressionModelHist_ = load_model(regressionHistModel.c_str());
    regressionModelHog_ = load_model(regressionHogModel.c_str());

    // re-identification experiment
    begin = clock();

    for (Car * itCar : BcarVec) {
        itCar->findIn(&Amap, regressionModelHist_, regressionModelHog_, &tmpCarA);
        if (tmpCarA == nullptr) {
            notFoundCount++;
            continue;
        }
        if (itCar->ID == tmpCarA->myTwin) {
            cout << " POSITIVE\n";
            foundCount++;
        } else {
            cout << " FALSE-POSITIVE\n";
            falseFound++;
        }
    }
    end = clock();
    double elapsed_secs2 = double(end - begin) / CLOCKS_PER_SEC;

    // printing output informations
    cout << "*** DONE ***\n";
    cout << "*** RESULTS ***\n";
    cout << "** SEARCHING IN " << Amap.size() << " CARS **\n";
    cout << "** " << (float) foundCount / BcarVec.size() * 100 << "% (" << foundCount << '/' << BcarVec.size() << ") cars successfully found **\n";
    cout << "** " << (float) notFoundCount / BcarVec.size() * 100 << "% (" << notFoundCount << '/' << BcarVec.size() << ") cars not found **\n";
    cout << "** " << (float) falseFound / BcarVec.size() * 100 << "% (" << falseFound << '/' << BcarVec.size() << ") cars false-positive found **\n";
    cout << "** " << elapsed_secs2 << " seconds - reidentification time " << "**\n";
    cout << "** " << elapsed_secs2 / (float) BcarVec.size() *1000 << " ms/car - reidentification time " << "**\n";
    cout << "** " << elapsed_secs1 << " seconds - computation time " << "**\n";
    cout << "** " << elapsed_secs1 / (float) (Amap.size() + Bmap.size())*1000 << " ms/car - computation time " << "**\n";
    cout << "** " << elapsed_secs1 / (float) (Amap.size() + Bmap.size())*1000 + elapsed_secs2 / (float) BcarVec.size() *1000 << " ms/car - overall time " << "**\n";
    cout << "** HISTOGRAM MODEL NAME: " << regressionHistModel << "**\n";
    cout << "** HOG MODEL NAME: " << regressionHogModel << "**\n";

    // releasing sources
    free_and_destroy_model(&regressionModelHist_);
    free_and_destroy_model(&regressionModelHog_);
    releaseSources();

    return 0;
}