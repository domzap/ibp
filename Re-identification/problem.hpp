/**
 * Thesis name: Vehicle Re-identification in Video
 * File name: problem.hpp
 * Written by Dominik Zapletal <xzaple34@stud.fit.vutbr.cz>, May 2015
 */

#ifndef PROBLEM_HPP
#define	PROBLEM_HPP

#include "linear.h"
#include <vector>

using namespace std;

class Problem {
private:
    int totalMaxIndex = 0;
    vector<vector<struct feature_node>> x_vec;

public:
    vector<struct feature_node *> x;
    vector<double> y;
    struct problem prob;
    void newTd(double label);
    static void addFeature(int index, double value, vector<feature_node>& x);
    vector<struct feature_node>* getLastXvec();
    void endTd(int tmpMaxIndex);
    void finalize();

};

#endif	/* PROBLEM_HPP */

