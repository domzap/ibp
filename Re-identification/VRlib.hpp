/**
 * Thesis name: Vehicle Re-identification in Video
 * File name: VRlib.hpp
 * Written by Dominik Zapletal <xzaple34@stud.fit.vutbr.cz>, May 2015
 */

#ifndef VRLIB_HPP
#define	VRLIB_HPP

#include "car.hpp"

using namespace std;
using namespace cv;

double varianceOfLaplacian(const cv::Mat& src);
bool exists(const std::string& name);
void databaseInit(unordered_map<int, Car*>& Amap,
        unordered_map<int, Car*>& Bmap,
        struct model* regressionModelHist_,
        struct model* regressionModelHog_,
        struct model* classificationModelHist_,
        struct model* classificationModelHog_,
        string dbFileName);
void databaseUpdate(unordered_map<int, Car*>* Amap,
        unordered_map<int, Car*>* Bmap,
        model* regressionModelHist_,
        model* regressionModelHog_,
        string dbFileName,
        int regAmount);
void regressMatCollection(map<double, pair < Car *, Car*>>& regMap, Mat& outputMat);
Mat getRegScaleMat(unordered_map<int, Car*>& Amap, unordered_map<int, Car*>& Bmap, model* ModelHist_, model* ModelHog_);
double vecMed(vector<double>& vec);
void getHist(Mat& src);
void drawHist(Mat& src, int divX, int divY, int scale);
float histsCompare(Mat& src1, Mat& src2, int method);
float histsCompare2(Mat& src1, Mat& src2, int method);
void histCmp(vector<float>& histCompVec, Mat& src1, Mat& src2, int divX, int divY, int method);
void fillHashs(unordered_map<int, Car*>& Amap, unordered_map<int, Car*>& Bmap,ifstream& matchFile, int& carMaxID, int carIDfloor);
void printHistValues(Car * carA, Car * carB);
bool findAndReturn(const unordered_map<int, Car*>* map, int key, Car ** value);
bool verifyPNG(string in);
int randomXY(int,int);
double random01();
int myRandom(int s, int e);
Mat get_hogdescriptor_visual_image(Mat& origImg,
                                   vector<float>& descriptorValues,
                                   Size winSize,
                                   Size cellSize,                                   
                                   int scaleFactor,
                                   double viz_factor);

#endif	/* VRLIB_HPP */
