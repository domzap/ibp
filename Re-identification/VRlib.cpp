/**
 * Thesis name: Vehicle Re-identification in Video
 * File name: VRlib.cpp
 * Description: Various function implementations from VRlib.hpp.
 * Written by Dominik Zapletal <xzaple34@stud.fit.vutbr.cz>, May 2015
 */

/*Libraries*/
#include <dirent.h> 
#include <fstream>
#include <random>
#include <iostream>
#include <map>
#include <sys/stat.h>
#include "VRlib.hpp"

/**
 * Returns an exsitence of a file.
 * @param name
 * @return 
 */
bool exists(const std::string& name) {
    struct stat buffer;
    return (stat(name.c_str(), &buffer) == 0);
}

/**
 * This function is used for created webpage database initialization. Regression
 * from all possible A, B map combinations is computed.
 * @param Amap
 * @param Bmap
 * @param regressionModelHist_
 * @param regressionModelHog_
 * @param classificationModelHist_
 * @param classificationModelHog_
 * @param dbFileName
 */
void databaseInit(unordered_map<int, Car*>& Amap,
        unordered_map<int, Car*>& Bmap,
        struct model* regressionModelHist_,
        struct model* regressionModelHog_,
        struct model* classificationModelHist_,
        struct model* classificationModelHog_,
        string dbFileName) {
    // variables
    Car* tmpCarA;
    Car* tmpCarB;
    stringstream tmpString;
    double tmpRegressionResult;
    double tmpClassificationResult;
    int tmpID = 0;
    ofstream sqlFile(dbFileName);

    // database initialization
    sqlFile << "DROP TABLE USER_VALUE;\n"
            << "DROP TABLE VEHICLE_PAIR;\n\n"
            << "CREATE TABLE VEHICLE_PAIR(\n"
            << "    ID_OF_PAIR INT(10) NOT NULL AUTO_INCREMENT,\n"
            << "    IMG_PATH_A VARCHAR(50) NOT NULL,\n"
            << "    IMG_PATH_B VARCHAR(50) NOT NULL,\n"
            << "    ID_OF_A INT(10) NOT NULL,\n"
            << "    ID_OF_B INT(10) NOT NULL,\n"
            << "    AVG_REGRESSION_VALUE FLOAT NOT NULL,\n"
            << "    AVG_CLASSIFICATION_VALUE FLOAT NOT NULL,\n"
            << "    PRIMARY KEY (ID_OF_PAIR));\n\n"
            << "CREATE TABLE USER_VALUE(\n"
            << "    ID_OF_PAIR INTEGER NOT NULL,\n"
            << "    VALUE INTEGER NOT NULL,\n"
            << "    FOREIGN KEY (ID_OF_PAIR) REFERENCES VEHICLE_PAIR(ID_OF_PAIR));\n\n";
    sqlFile << "INSERT INTO VEHICLE_PAIR (ID_OF_PAIR, IMG_PATH_A, IMG_PATH_B, ID_OF_A, ID_OF_B, AVG_REGRESSION_VALUE, AVG_CLASSIFICATION_VALUE) VALUES\n";
    int overAll = (Amap.size())*(Bmap.size()); // all possible combinations
    int progress = 0;

    // processing all cars
    for (auto itCarA = Amap.begin(); itCarA != Amap.end(); ++itCarA) {
        for (auto itCarB = Bmap.begin(); itCarB != Bmap.end(); ++itCarB) {
            ++progress;
            tmpCarB = itCarB->second;
            tmpCarA = itCarA->second;
            tmpRegressionResult = tmpCarB->regressionWith(tmpCarA, regressionModelHist_, regressionModelHog_, 25);
            if (tmpRegressionResult > 0.0) { // positive regression results only
                if (tmpID != 0) { // no start nor end
                    sqlFile << ",\n";
                }
                tmpClassificationResult = tmpCarB->regressionWith(tmpCarA, classificationModelHist_, classificationModelHog_, 25);
                tmpString.str("");
                tmpString.clear();

                // saving average classification/regression results
                sqlFile << "(" << ++tmpID << ", " << "'img_database/" << tmpID << "_A_" << tmpCarA->getID() << ".png', " << "'img_database/" << tmpID << "_B_" << tmpCarB->getID() << ".png', "
                        << tmpCarA->getID() << ", " << tmpCarB->getID() << ", " << tmpRegressionResult << ", " << tmpClassificationResult << ")"; //TODO changle path!!
                tmpString.str("");
                tmpString.clear();

                // saving squared images for image database
                tmpString << "./img_database/" << tmpID << "_A_" << tmpCarA->getID() << ".png";
                imwrite(tmpString.str(), tmpCarA->squared);
                tmpString.str("");
                tmpString.clear();
                tmpString << "./img_database/" << tmpID << "_B_" << tmpCarB->getID() << ".png";
                imwrite(tmpString.str(), tmpCarB->squared);
                cout << progress << "/" << overAll << " " << tmpRegressionResult << " " << tmpClassificationResult << "\n";
            }
        }
    }
    sqlFile << ";\n"; // end of SQL command
    sqlFile.close(); // closing .sql file
}

/**
 * Extracts data from imput CSV line.
 * @param csvin
 * @param ID
 * @param numberA
 * @param numberB
 * @return 
 */
bool getCSVData(ifstream& csvin, int& ID, int& numberA, int& numberB) {
    string line;
    stringstream aData;
    getline(csvin, line);
    aData.str(line);
    if (!csvin.eof()) {

        aData >> ID;
        // finding next comma
        while (aData.peek() != ',') {
            aData.ignore();
        }
        if (aData.peek() == ',') {
            aData.ignore();
        }
        // finding next comma
        while (aData.peek() != ',') {
            aData.ignore();
        }
        if (aData.peek() == ',') {
            aData.ignore();
        }
        // finding next comma
        while (aData.peek() != ',') {
            aData.ignore();
        }
        if (aData.peek() == ',') {
            aData.ignore();
        }
        aData >> numberA;
        // finding next comma
        while (aData.peek() != ',') {
            aData.ignore();
        }
        if (aData.peek() == ',') {
            aData.ignore();
        }
        aData >> numberB;
        //cout << ID << " " << numberA << " " << numberB << "\n";
        return true;
    } else {
        return false;
    }
}

/**
 * Updates existing database from CSV file.
 * @param Amap
 * @param Bmap
 * @param regressionModelHist_
 * @param regressionModelHog_
 * @param dbFileName
 * @param regAmount
 */
void databaseUpdate(unordered_map<int, Car*>* Amap,
        unordered_map<int, Car*>* Bmap,
        model* regressionModelHist_,
        model* regressionModelHog_,
        string dbFileName, int regAmount) {

    Car* tmpCarA = nullptr;
    Car* tmpCarB = nullptr;
    int carIDA = -1;
    int carIDB = -1;
    double tmpRegressionResult;
    int pairID = -1;

    ofstream sqlFile(dbFileName); // creating .sql file
    ifstream csvin("xzaple34.csv"); // opening .csv file of existing database
    if (!csvin.is_open()) {
        cout << "Cannot open the csv file!" << endl;
        return;
    }
    // parsing CSV file and creating SQL command for database update
    while (getCSVData(csvin, pairID, carIDA, carIDB)) {
        if (!(findAndReturn(Amap, carIDA, &tmpCarA) && findAndReturn(Bmap, carIDB, &tmpCarB))) {
            cout << "CAR NOT FOUND\n";
            break;
        }
        tmpRegressionResult = tmpCarB->regressionWith(tmpCarA, regressionModelHist_, regressionModelHog_, regAmount);
        sqlFile << "UPDATE VEHICLE_PAIR SET AVG_REGRESSION_VALUE = " << tmpRegressionResult << " WHERE ID_OF_PAIR = " << pairID << ";\n";
    }
    sqlFile.close();
    csvin.close();
}

/**
 * Adds combined image source to output image with regression result.
 * @param src
 * @param regression
 * @param pos
 * @param outputMat
 */
void add2OutputMat(Mat& src, double regression, int pos, Mat& outputMat) {
    int frameSize = 5; // size of space between image objects
    if (pos == 1) { // new line image
        // double to string conversion
        string str2Print = static_cast<ostringstream*> (&(ostringstream() << regression))->str();
        if (str2Print.size() > 8) {
            str2Print = regression < 0 ? str2Print.substr(0, 8) : str2Print.substr(0, 7);
        }
        
        // resizing current Mat, adding combined image, adding regression result
        int baseline;
        Size textSize = getTextSize("-0.54129", FONT_HERSHEY_SIMPLEX, 0.7, 1, &baseline);
        Mat regMat = cvCreateMat(textSize.height + 8, textSize.width + 8, src.type());
        regMat = Scalar(255, 255, 255, 0);
        textSize = getTextSize(str2Print, FONT_HERSHEY_SIMPLEX, 0.7, 1, &baseline);
        putText(regMat, str2Print, cvPoint(regMat.cols / 2 - textSize.width / 2, regMat.rows / 2 + textSize.height / 2), FONT_HERSHEY_SIMPLEX, 0.7, cvScalar(0, 0, 0), 1, CV_AA);
        Mat tmpMat = cvCreateMat(outputMat.rows + src.rows + 2 * frameSize, src.cols * 2 + 6 * frameSize + regMat.cols, src.type());
        tmpMat = Scalar(255, 255, 255, 0);
        Mat subMat = tmpMat(Rect(0, 0, outputMat.cols, outputMat.rows));
        outputMat.copyTo(subMat);
        subMat = tmpMat(Rect(frameSize, outputMat.rows + frameSize, src.cols, src.rows));
        src.copyTo(subMat);
        subMat = tmpMat(Rect(3 * frameSize + src.cols, outputMat.rows + frameSize + src.rows / 2 - regMat.rows / 2, regMat.cols, regMat.rows));
        regMat.copyTo(subMat);
        tmpMat.copyTo(outputMat);
    } else { 
        // adding combined image to existing line
        Mat subMat = outputMat(Rect(outputMat.cols - frameSize - src.cols, outputMat.rows - frameSize - src.rows, src.cols, src.rows));
        src.copyTo(subMat);
    }
}

/**
 * Creates image collection of combined images with regression results in 
 * between.
 * @param regMap
 * @param outputMat
 */
void regressMatCollection(map<double, pair < Car *, Car*>>&regMap, Mat& outputMat) {
    auto iter = regMap.end(); // from the highest reggresion result
    auto prevIter = iter;
    iter--;
    int cnt = 1;
    Mat tmpMat;
    outputMat = Scalar(255, 255, 255, 0);
    for (double x = 1.0; true; x *= 1.1 /*exponential step*/) {
        while (cnt != round(x)) {
            iter--;
            cnt++;
            if (iter == regMap.begin())
                goto end;
        }
        if (prevIter == iter) {
            continue;
        }
        
        // add combined images to output Mat
        cout << iter->first << '\n';
        tmpMat = iter->second.first->getSharpestMat();
        add2OutputMat(tmpMat, iter->first, 1, outputMat);
        tmpMat = iter->second.second->getSharpestMat();
        add2OutputMat(tmpMat, iter->first, 2, outputMat);
        prevIter = iter;
    }
end:
    return;
}

/**
 * Creates reggression map ordered by reggression result and generates
 * regression scaled image.
 * @param Amap
 * @param Bmap
 * @param ModelHist_
 * @param ModelHog_
 * @return 
 */
Mat getRegScaleMat(unordered_map<int, Car*>& Amap, unordered_map<int, Car*>& Bmap, model* ModelHist_, model* ModelHog_) {
    Mat outputMap;
    map<double, pair < Car *, Car*>> regMap;
    int cnt = 0;
    int amount = Amap.size() * Bmap.size();
    for (auto itA = Amap.begin(); itA != Amap.end(); ++itA) {
        for (auto itB = Bmap.begin(); itB != Bmap.end(); ++itB) {
            regMap.insert(make_pair(itA->second->regressionWith(itB->second, ModelHist_, ModelHog_, 20), make_pair(itA->second, itB->second)));
            cout << ++cnt << "/" << amount << endl;
        }
    }
    regressMatCollection(regMap, outputMap);
    return outputMap;
}

/**
 * Returns 90. percentile of a vector.
 * @param vec
 * @return 
 */
double vecMed(vector<double>& vec) {
    if (vec.empty()) return 0;
    else {
        sort(vec.begin(), vec.end());
        return vec.at(static_cast<int> (0.9 * (vec.size() - 1)));
        /*auto first = vec.begin() + 2;
        auto last = vec.end() - 2;
        vector<double> subVec(first, last);
        if (subVec.size() % 2 == 0)
            return (subVec[subVec.size() / 2 - 1] + subVec[subVec.size() / 2]) / 2;
        else
            return subVec[subVec.size() / 2];*/
    }
}

/**
 * Generates random number between input intervals.
 * @param i1
 * @param i2
 * @return 
 */
int randomXY(int i1, int i2) {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis(i1, i2);
    return static_cast<int> (dis(gen));
}

/**
 * Generates random number between 0 and 1.
 * @return 
 */
double random01() {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis(0, 1);
    return dis(gen);
}

/**
 * Draws histogram into the image.
 * @param src
 * @param w
 * @param h
 * @return 
 */
void getHist(Mat& src) {
    /// Separate the image in 3 places ( B, G and R )
    vector<Mat> bgr_planes;
    split(src, bgr_planes);

    /// Establish the number of bins
    int histSize = 16;

    /// Set the ranges ( for B,G,R) )
    float range[] = {0, 256};
    const float* histRange = {range};

    bool uniform = true;
    bool accumulate = false;

    Mat b_hist, g_hist, r_hist;

    /// Compute the histograms:
    calcHist(&bgr_planes[0], 1, 0, Mat(), b_hist, 1, &histSize, &histRange,
            uniform, accumulate);
    calcHist(&bgr_planes[1], 1, 0, Mat(), g_hist, 1, &histSize, &histRange,
            uniform, accumulate);
    calcHist(&bgr_planes[2], 1, 0, Mat(), r_hist, 1, &histSize, &histRange,
            uniform, accumulate);

    // Draw the histograms for B, G and R
    int hist_w = src.cols;
    int hist_h = src.rows;
    double bin_w = (double) hist_w / histSize;

    Mat histImage(hist_h, hist_w, CV_8UC3, Scalar(0, 0, 0, 0));

    /// Normalize the result to [ 0, histImage.rows ]
    normalize(b_hist, b_hist, 0, histImage.rows, NORM_MINMAX, -1, Mat());
    normalize(g_hist, g_hist, 0, histImage.rows, NORM_MINMAX, -1, Mat());
    normalize(r_hist, r_hist, 0, histImage.rows, NORM_MINMAX, -1, Mat());

    /// Draw for each channel
    for (int i = 1; i < histSize; i++) {
        line(
                src,
                Point(cvRound(bin_w * (i - 1)),
                hist_h - cvRound(b_hist.at<float> (i - 1))),
                Point(cvRound(bin_w * (i)), hist_h - cvRound(b_hist.at<float> (i))),
                Scalar(255, 0, 0), 2, CV_AA, 0);
    }
    for (int i = 1; i < histSize; i++) {
        line(
                src,
                Point(cvRound(bin_w * (i - 1)),
                hist_h - cvRound(g_hist.at<float> (i - 1))),
                Point(cvRound(bin_w * (i)), hist_h - cvRound(g_hist.at<float> (i))),
                Scalar(0, 255, 0), 2, CV_AA, 0);
    }
    for (int i = 1; i < histSize; i++) {
        line(
                src,
                Point(cvRound(bin_w * (i - 1)),
                hist_h - cvRound(r_hist.at<float> (i - 1))),
                Point(cvRound(bin_w * (i)), hist_h - cvRound(r_hist.at<float> (i))),
                Scalar(0, 0, 255), 2, CV_AA, 0);
    }
}

/**
 * Calculates image sharpness. OpenCV port of 'LAPV' algorithm (Pech2000).
 * (http://stackoverflow.com/questions/7765810/is-there-a-way-to-detect-if-an-image-is-blurry)
 * @param src
 * @return 
 */
double varianceOfLaplacian(const cv::Mat& src) {
    cv::Mat lap;
    cv::Laplacian(src, lap, CV_64F);
    cv::Scalar mu, sigma;
    cv::meanStdDev(lap, mu, sigma);
    double focusMeasure = sigma.val[0] * sigma.val[0];
    return focusMeasure;
}

/**
 * Draws histograms in the source image based on input divisions (grid).
 * @param src
 * @param divX
 * @param divY
 */
void drawHist(Mat& src, int divX, int divY, int scale) {
    resize(src, src, Size(src.cols*scale, src.rows * scale));
    int window_width = src.cols;
    int window_height = src.rows;
    Size smallSize(window_width / divX, window_height / divY);
    for (int y = 0; (y + smallSize.height - 1) < window_height; y
            += smallSize.height) {
        for (int x = 0; (x + smallSize.width - 1) < window_width; x
                += smallSize.width) {
            Rect rect = Rect(x, y, smallSize.width, smallSize.height);
            line(src, Point(x, y), Point(x, y + smallSize.height),
                    Scalar(0, 0, 0), 1, CV_AA, 0);
            Mat currentImagePart = Mat(src, rect);
            getHist(currentImagePart);
        }
        line(src, Point(0, y), Point(0 + window_width, y), Scalar(0, 0, 0), 1,
                CV_AA, 0);
    }
}

/**
 * Compares two whole histograms and returns one float number. Comparison based
 * on input method.
 * @param src1
 * @param src2
 * @param method
 * @return 
 */
float histsCompare(Mat& src1, Mat& src2, int method) {
    Mat hsv_src1, hsv_src2;

    /// Convert to HSV
    cvtColor(src1, hsv_src1, COLOR_BGR2HSV);
    cvtColor(src2, hsv_src2, COLOR_BGR2HSV);

    /// Using 50 bins for hue and 60 for saturation
    int h_bins = 50;
    int s_bins = 60;
    int histSize[] = {h_bins, s_bins};

    // hue varies from 0 to 179, saturation from 0 to 255
    float h_ranges[] = {0, 180};
    float s_ranges[] = {0, 256};

    const float* ranges[] = {h_ranges, s_ranges};

    // Use the o-th and 1-st channels
    int channels[] = {0, 1};

    /// Histograms
    MatND hist_src1;
    MatND hist_src2;

    /// Calculate the histograms for the HSV images
    calcHist(&hsv_src1, 1, channels, Mat(), hist_src1, 2, histSize, ranges,
            true, false);
    normalize(hist_src1, hist_src1, 0, 1, NORM_MINMAX, -1, Mat());

    calcHist(&hsv_src2, 1, channels, Mat(), hist_src2, 2, histSize, ranges,
            true, false);
    normalize(hist_src2, hist_src2, 0, 1, NORM_MINMAX, -1, Mat());

    double result;
    result = compareHist(hist_src1, hist_src2, method);

    return result;
}

/**
 * Compares two whole histograms and returns one float number. Comparison based
 * on input method. BGR channels are compared separately. Mean float number returned.
 * @param src1
 * @param src2
 * @param method
 * @return 
 */
float histsCompare2(Mat& src1, Mat& src2, int method) {
    /// Separate the image in 3 places ( B, G and R )
    vector<Mat> bgr_planes1;
    vector<Mat> bgr_planes2;

    split(src1, bgr_planes1);
    split(src2, bgr_planes2);

    /// Establish the number of bins
    int histSize = 256;

    /// Set the ranges ( for B,G,R) )
    float range[] = {0, 256};
    const float* histRange = {range};

    Mat b_hist1, g_hist1, r_hist1;
    Mat b_hist2, g_hist2, r_hist2;

    /// Compute the histograms:
    calcHist(&bgr_planes1[0], 1, 0, Mat(), b_hist1, 1, &histSize, &histRange,
            true, false);
    calcHist(&bgr_planes1[1], 1, 0, Mat(), g_hist1, 1, &histSize, &histRange,
            true, false);
    calcHist(&bgr_planes1[2], 1, 0, Mat(), r_hist1, 1, &histSize, &histRange,
            true, false);

    calcHist(&bgr_planes2[0], 1, 0, Mat(), b_hist2, 1, &histSize, &histRange,
            true, false);
    calcHist(&bgr_planes2[1], 1, 0, Mat(), g_hist2, 1, &histSize, &histRange,
            true, false);
    calcHist(&bgr_planes2[2], 1, 0, Mat(), r_hist2, 1, &histSize, &histRange,
            true, false);


    /// Normalize the result to [ 0, 1 ]
    normalize(b_hist1, b_hist1, 0, 1, NORM_MINMAX, -1, Mat());
    normalize(g_hist1, g_hist1, 0, 1, NORM_MINMAX, -1, Mat());
    normalize(r_hist1, r_hist1, 0, 1, NORM_MINMAX, -1, Mat());

    normalize(b_hist2, b_hist2, 0, 1, NORM_MINMAX, -1, Mat());
    normalize(g_hist2, g_hist2, 0, 1, NORM_MINMAX, -1, Mat());
    normalize(r_hist2, r_hist2, 0, 1, NORM_MINMAX, -1, Mat());

    double result;
    result = compareHist(b_hist1, b_hist2, method);
    result += compareHist(g_hist1, g_hist2, method);
    result += compareHist(r_hist1, r_hist2, method);

    return result / 3.0;
}

/**
 * Creates hitogram comparison vectors.
 * @param src1
 * @param src2
 * @param divX
 * @param divY
 * @param method
 */
void histCmp(vector<float>& histCompVec, Mat& src1, Mat& src2, int divX, int divY, int method) {
    int window_width = src1.cols;
    int window_height = src1.rows;
    histCompVec.clear();
    int i = 0;
    Size smallSize(window_width / divX, window_height / divY);
    for (int y = 0; (y + smallSize.height - 1) < window_height; y
            += smallSize.height / 2) {
        for (int x = 0; (x + smallSize.width - 1) < window_width; x
                += smallSize.width / 2) {
            Rect rect = Rect(x, y, smallSize.width, smallSize.height);
            Mat currentImagePart1 = Mat(src1, rect);
            Mat currentImagePart2 = Mat(src2, rect);

            histCompVec.push_back(
                    histsCompare2(currentImagePart1, currentImagePart2,
                    method));
        }
    }
}

/**
 * Verifys .png filename input.
 * @param in
 * @return 
 */
bool verifyPNG(string in) {
    if (in.find("_combined.png") != string::npos)
        return true;
    else
        return false;
}

/**
 * Generates random number
 * @param start
 * @param end
 * @return 
 */
int myRandom(int start, int end) {
    return randomXY(start, end - 1);
}

/**
 * Searching in unordered map.
 * @param map
 * @param key
 * @param value
 * @return 
 */
bool findAndReturn(const unordered_map<int, Car*>* map, int key, Car ** value) {
    auto t = map->find(key);
    if (t == map->end()) return false;
    *value = t->second;
    return true;
}

/**
 * Fills unordered maps with cars from matching files.
 * @param matchFile
 * @param carMaxID
 * @param carIDfloor
 */
void fillHashs(unordered_map<int, Car*>& Amap, unordered_map<int, Car*>& Bmap, ifstream& matchFile, int& carMaxID, int carIDfloor) {
    carMaxID = 0;
    unordered_map<int, Car*> * currMap;
    int carID = -1;
    int frame = -1;
    string pngFileName;
    Car * tmpCar;
    string line;
    // filling hash tables with cars
    getline(matchFile, line);
    for (int a = 0; a < 2; a++) {
        getline(matchFile, line);
        if (a == 0) {
            currMap = &Amap;
            cout << "** folder A **\n";
        } else {
            currMap = &Bmap;
            cout << "** folder B **\n";
        }
        DIR *dir = opendir(line.c_str());
        if (dir) {
            struct dirent *ent;
            while ((ent = readdir(dir)) != NULL) {
                pngFileName = ent->d_name;
                if (verifyPNG(pngFileName)) {
                    carID = atoi(pngFileName.substr(0, pngFileName.find("_")).c_str()) + carIDfloor;
                    frame = atoi(pngFileName.substr(pngFileName.find("_") + 1, pngFileName.rfind("_")).c_str());
                    if (findAndReturn(currMap, carID, &tmpCar)) {
                        tmpCar->push(string(line + '/' + pngFileName), frame);
                    } else {
                        tmpCar = new Car();
                        tmpCar->setID(carID);
                        tmpCar->push(string(line + '/' + pngFileName), frame);
                        currMap->emplace(carID, tmpCar);
                        if ((carID - carIDfloor) > carMaxID)
                            carMaxID = carID - carIDfloor;
                    }
                } else
                    continue;
            }
        } else {
            cout << "\n!!! Error opening directory !!!\n";
            exit(-1);
        }
        cout << "* " << currMap->size() << " CARS IN MAP (overall) *\n";
    }
}

/**
 * HOGDescriptor visual_imagealizer. Adapted for arbitrary size of feature sets and training images.
 * (http://www.juergenwiki.de/work/wiki/doku.php?id=public:hog_descriptor_computation_and_visualization#visualizing_the_hogdescriptor_c)
 * @param origImg
 * @param descriptorValues
 * @param winSize
 * @param cellSize
 * @param scaleFactor
 * @param viz_factor
 * @return 
 */ 
Mat get_hogdescriptor_visual_image(Mat& origImg,
        vector<float>& descriptorValues,
        Size winSize,
        Size cellSize,
        int scaleFactor,
        double viz_factor) {
    Mat visual_image;
    resize(origImg, visual_image, Size(origImg.cols*scaleFactor, origImg.rows * scaleFactor));
    cvtColor(visual_image, visual_image, CV_RGB2GRAY);
    cvtColor(visual_image, visual_image, CV_GRAY2RGB);

    int gradientBinSize = 9;
    // dividing 180° into 9 bins, how large (in rad) is one bin?
    float radRangeForOneBin = 3.14 / (float) gradientBinSize;

    // prepare data structure: 9 orientation / gradient strenghts for each cell
    int cells_in_x_dir = winSize.width / cellSize.width;
    int cells_in_y_dir = winSize.height / cellSize.height;
    int totalnrofcells = cells_in_x_dir * cells_in_y_dir;
    float*** gradientStrengths = new float**[cells_in_y_dir];
    int** cellUpdateCounter = new int*[cells_in_y_dir];
    for (int y = 0; y < cells_in_y_dir; y++) {
        gradientStrengths[y] = new float*[cells_in_x_dir];
        cellUpdateCounter[y] = new int[cells_in_x_dir];
        for (int x = 0; x < cells_in_x_dir; x++) {
            gradientStrengths[y][x] = new float[gradientBinSize];
            cellUpdateCounter[y][x] = 0;

            for (int bin = 0; bin < gradientBinSize; bin++)
                gradientStrengths[y][x][bin] = 0.0;
        }
    }

    // nr of blocks = nr of cells - 1
    // since there is a new block on each cell (overlapping blocks!) but the last one
    int blocks_in_x_dir = cells_in_x_dir - 1;
    int blocks_in_y_dir = cells_in_y_dir - 1;

    // compute gradient strengths per cell
    int descriptorDataIdx = 0;
    int cellx = 0;
    int celly = 0;

    for (int blockx = 0; blockx < blocks_in_x_dir; blockx++) {
        for (int blocky = 0; blocky < blocks_in_y_dir; blocky++) {
            // 4 cells per block ...
            for (int cellNr = 0; cellNr < 4; cellNr++) {
                // compute corresponding cell nr
                int cellx = blockx;
                int celly = blocky;
                if (cellNr == 1) celly++;
                if (cellNr == 2) cellx++;
                if (cellNr == 3) {
                    cellx++;
                    celly++;
                }

                for (int bin = 0; bin < gradientBinSize; bin++) {
                    float gradientStrength = descriptorValues[ descriptorDataIdx ];
                    descriptorDataIdx++;

                    gradientStrengths[celly][cellx][bin] += gradientStrength;

                } // for (all bins)


                // note: overlapping blocks lead to multiple updates of this sum!
                // we therefore keep track how often a cell was updated,
                // to compute average gradient strengths
                cellUpdateCounter[celly][cellx]++;

            } // for (all cells)


        } // for (all block x pos)
    } // for (all block y pos)


    // compute average gradient strengths
    for (int celly = 0; celly < cells_in_y_dir; celly++) {
        for (int cellx = 0; cellx < cells_in_x_dir; cellx++) {

            float NrUpdatesForThisCell = (float) cellUpdateCounter[celly][cellx];

            // compute average gradient strenghts for each gradient bin direction
            for (int bin = 0; bin < gradientBinSize; bin++) {
                gradientStrengths[celly][cellx][bin] /= NrUpdatesForThisCell;
            }
        }
    }


    cout << "descriptorDataIdx = " << descriptorDataIdx << endl;

    // draw cells
    for (int celly = 0; celly < cells_in_y_dir; celly++) {
        for (int cellx = 0; cellx < cells_in_x_dir; cellx++) {
            int drawX = cellx * cellSize.width;
            int drawY = celly * cellSize.height;

            int mx = drawX + cellSize.width / 2;
            int my = drawY + cellSize.height / 2;

            rectangle(visual_image,
                    Point(drawX*scaleFactor, drawY * scaleFactor),
                    Point((drawX + cellSize.width) * scaleFactor,
                    (drawY + cellSize.height) * scaleFactor),
                    CV_RGB(0, 0, 0),
                    1);

            // draw in each cell all 9 gradient strengths
            for (int bin = 0; bin < gradientBinSize; bin++) {
                float currentGradStrength = gradientStrengths[celly][cellx][bin];

                // no line to draw?
                if (currentGradStrength == 0)
                    continue;

                float currRad = bin * radRangeForOneBin + radRangeForOneBin / 2;

                float dirVecX = cos(currRad);
                float dirVecY = sin(currRad);
                float maxVecLen = cellSize.width / 2;
                float scale = viz_factor; // just a visual_imagealization scale,
                // to see the lines better

                // compute line coordinates
                float x1 = mx - dirVecX * currentGradStrength * maxVecLen * scale;
                float y1 = my - dirVecY * currentGradStrength * maxVecLen * scale;
                float x2 = mx + dirVecX * currentGradStrength * maxVecLen * scale;
                float y2 = my + dirVecY * currentGradStrength * maxVecLen * scale;

                // draw gradient visual_imagealization
                line(visual_image,
                        Point(x1*scaleFactor, y1 * scaleFactor),
                        Point(x2*scaleFactor, y2 * scaleFactor),
                        CV_RGB(0, 255, 0),
                        1, CV_AA, 0);

            } // for (all bins)

        } // for (cellx)
    } // for (celly)


    // don't forget to free memory allocated by helper data structures!
    for (int y = 0; y < cells_in_y_dir; y++) {
        for (int x = 0; x < cells_in_x_dir; x++) {
            delete[] gradientStrengths[y][x];
        }
        delete[] gradientStrengths[y];
        delete[] cellUpdateCounter[y];
    }
    delete[] gradientStrengths;
    delete[] cellUpdateCounter;

    return visual_image;
}