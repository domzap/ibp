/**
 * Thesis name: Vehicle Re-identification in Video
 * File name: car.hpp
 * Written by Dominik Zapletal <xzaple34@stud.fit.vutbr.cz>, May 2015
 */

#ifndef CAR_HPP
#define	CAR_HPP

/*Libraries*/
#include <unordered_map>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/opencv.hpp"
#include "problem.hpp"

/*Namespaces*/
using namespace std;
using namespace cv;

class Combined;

/*Extended Car class*/
class Car {
private:
    /*Private Variables*/
    // vector of Combined instances (combined images)
    vector<Combined*> combinedVec;
    // current car reggression result
    double appeared = 0;

public:
    /*Public Variables*/
    Mat squared;
    list<Combined*> sharp; // list of sharpest combined images
    
    // mean feature vectors
    vector<float> meanHistVec;
    vector<float> meanHogVec;
    
    // feature vector lengths
    static int histValsNum;
    static int hogValsNum;
    
    static vector<struct feature_node> x;
    int myTwin = -1; // double's ID
    int ID;
    
    /*Public Methods*/
    ~Car();
    void push(string p, int f);
    Combined * getRandomCombined();
    Mat getRandomMat();
    Mat getSharpestMat();
    vector<float> * getRandomHistVec();
    vector<float> * getRandomHogVec();
    vector<float> * getSharpestHistVec();
    vector<float> * getSharpestHogVec();
    int getSize();
    void setID(int id);
    int getID();
    void compareVecWith(Car * carX, Problem& p, bool HOG, double label);
    void findIn(unordered_map<int, Car*>* map, struct model* modelHist_, struct model* modelHog_, Car ** tmpCar);
    double regressionWith(Car * cmpCar, model* modelHist_, model* modelHog_, int repeat);
    void addFeatures(vector<float>* vecA, vector<float>* vecB, feature_node * x);
    int addFeatures(vector<float>* vecA, vector<float>* vecB, vector<feature_node>& x);
};

/*Combined image class*/
class Combined {
private:
    /*Private Variables*/
    int frame; // combined image frame
    Mat combined; // combined image
    string path2im; // path to combined image
    Size reSize = Size(312, 104); // combined image size
    
    // combined image feature vectors
    vector<float> hogVec;
    vector<float> histVec;

public:
    /*Public Variables*/
    double sharpness = 0.0; // sharpness of combined image
    Car * belong2car; // pointer to parent car
    static bool training;

    /*Public Methods*/
    void releaseComb();
    void setFrame(int f);
    int getFrame();
    void pushCombined(string p);
    void computeHist(int divX, int divY, int bins, double overlap);
    void computeHog();
    void pushHistValues(Mat& cell, vector<float>& v, int bins);
    Mat getCombined();
    vector<float> * getHistVec();
    vector<float> * getHogVec();
};

#endif	/* CAR_HPP */

