/**
 * Thesis name: Vehicle Re-identification in Video
 * File name: car.cpp
 * Description: Here are implemented Car class methods.
 * Written by Dominik Zapletal <xzaple34@stud.fit.vutbr.cz>, May 2015
 */

/*Libraries*/
#include "car.hpp"
#include "VRlib.hpp"

/*Static variables initialization*/
vector<feature_node> Car::x;
int Car::histValsNum = -1;
int Car::hogValsNum = -1;

/**
 * Car destructor.
 */
Car::~Car() {
    for (int i = 0; i < combinedVec.size(); i++) {
        delete combinedVec[i];
    }
}

/**
 * Returns car ID.
 * @return 
 */
int Car::getID(){
    return ID;
}

/**
 * Creates a feature_node vector for regression. Optimalized for regression;
 * @param histVecA
 * @param histVecB
 * @param x - feature_node vector
 */
void Car::addFeatures(vector<float>* vecA, vector<float>* vecB, feature_node * x) {
    int size2 = vecA->size()*2;
    
    for (int i = 0; i < (int) vecA->size(); i++) {
        x[i].index = i + 1;
        x[i].value = vecA->at(i);
        x[i + vecA->size()].index = i + 1 + vecA->size();
        x[i + vecA->size()].value = vecB->at(i);
        x[i + size2].index = i + 1 + size2;
        x[i + size2].value = fabs(vecA->at(i) - vecB->at(i));
    }
    // end feature (-1,?)
    x[vecA->size()*3].index = -1;
    x[vecA->size()*3].value = 0.0;
}

/**
 * Creates a feature_node vector for regression. Non-optimalized for training.
 * @param vecA
 * @param vecB
 * @param x
 * @return 
 */
int Car::addFeatures(vector<float>* vecA, vector<float>* vecB, vector<feature_node>& x) {
    int cnt = 0;
    double val2Add = 0.0;
    struct feature_node newFN;
    // A car features
    for (int i = 0; i < (int) vecA->size(); i++, cnt++) {
        val2Add = vecA->at(i);
        if (val2Add == 0.0)
            continue;
        newFN.index = cnt + 1;
        newFN.value = val2Add;
        x.push_back(newFN);
    }
    // B car features
    for (int i = 0; i < (int) vecB->size(); i++, cnt++) {
        val2Add = vecB->at(i);
        if (val2Add == 0.0)
            continue;
        newFN.index = cnt + 1;
        newFN.value = val2Add;
        x.push_back(newFN);
    }
    // A-B car features
    for (int i = 0; i < (int) vecB->size(); i++, cnt++) {
        val2Add = fabs(vecA->at(i) - vecB->at(i));
        if (val2Add == 0.0)
            continue;
        newFN.index = cnt + 1;
        newFN.value = val2Add;
        x.push_back(newFN);
    }
    // end feature (-1,?)
    newFN.index = -1;
    newFN.value = 0.0;
    x.push_back(newFN);
    return cnt + 1;
}

/**
 * Calculates deep regression between two cars.
 * @param cmpCar
 * @param modelHist_
 * @param modelHog_
 * @param repeat
 * @return 
 */
double Car::regressionWith(Car * cmpCar, model* modelHist_, model* modelHog_, int repeat) {
    vector<double> tmpVals;
    double tmpHistVal;
    double tmpHogVal;
    Combined * cmpComb;
    Combined * cmpCombThis;
    for (int i = 0; i < repeat; i++) {
        cmpComb = cmpCar->getRandomCombined();
        cmpCombThis = getRandomCombined();
        addFeatures(cmpComb->getHistVec(), cmpCombThis->getHistVec(), &x[0]);
        tmpHistVal = predict(modelHist_, &x[0]);
        addFeatures(cmpComb->getHogVec(), cmpCombThis->getHogVec(), &x[0]);
        tmpHogVal = predict(modelHog_, &x[0]);
        tmpVals.push_back((tmpHistVal*0.4 + tmpHogVal*1.6)/2); // different weights for hist/hog
        //tmpVals.push_back((tmpHistVal + tmpHogVal)/2);
    }
    return vecMed(tmpVals); // returns median from all regressions
}

/**
 * Tries to reidentificate input B car in A car unordered map. Using histogram 
 * feature and HOG feature models.
 * 
 * @param map
 * @param modelHist_
 * @param modelHog_
 * @param tmpCar
 */
void Car::findIn(unordered_map<int, Car*>* map, model* modelHist_, model* modelHog_, Car ** tmpCar) {
    Car * currCar;
    vector<Car*> potentialMatchVec;
    double regThreshold = 0.30; // positive regression re-identification threshold
    
    // finding car in map
    for (auto c : *map) {
        currCar = c.second;
        // mean colour histogram feature vector regression
        addFeatures(&currCar->meanHistVec, &meanHistVec, &x[0]);
        if (predict(modelHist_, &x[0]) > regThreshold) {
            // mean HOG histogram feature vector regression 
            addFeatures(&currCar->meanHogVec, &meanHogVec, &x[0]);
            if (predict(modelHog_, &x[0]) > regThreshold) {
                // deep regression
                currCar->appeared = regressionWith(currCar, modelHist_, modelHog_, 21);
                if (currCar->appeared > regThreshold) {
                    // adding vehicle to the potential vehicle vector
                    potentialMatchVec.push_back(currCar);
                } else {
                    currCar->appeared = 0;
                }
            }
        }
    }
    
    if (potentialMatchVec.size() == 0) {
        *tmpCar = nullptr;
        cout << "POTENTIAL VECTOR EMPTY \n";
        return; // no vehicle found
    }
    
    // finding the best result
    cout << "POTENTIAL VECTOR SIZE: " << potentialMatchVec.size();
    double maxApp = -1;
    for (Car * tmpC : potentialMatchVec) {
        if (tmpC->appeared > maxApp) {
            maxApp = tmpC->appeared;
            *tmpCar = tmpC;
        }
        tmpC->appeared = 0;
    }
    if (maxApp <= 0.0) {
        cout << " CAR NOT FOUND\n";
        *tmpCar = nullptr;
        return;
    } else {
        cout << " CAR FOUND WITH SCORE " << maxApp;
    }
}

/**
 * Sets car id.
 * @param id
 */
void Car::setID(int id) {
    ID = id;
}

/**
 * Inserts new frame folder-filename information.
 * @param folder
 * @param fileName
 */
void Car::push(string path, int frame) {
    combinedVec.push_back(new Combined());
    combinedVec.back()->belong2car = this;
    combinedVec.back()->setFrame(frame);
    combinedVec.back()->pushCombined(path);
    if (sharp.size() == 0) {
        sharp.push_front(combinedVec.back());
    } else if (sharp.front()->sharpness < combinedVec.back()->sharpness) {
        sharp.push_front(combinedVec.back());
        if (sharp.size() > 3) {
            sharp.back()->releaseComb();
            sharp.pop_back();
        }
    } else {
        combinedVec.back()->releaseComb();
    }
    if (Car::x.size() == 0) {
        int u = (Car::hogValsNum > Car::histValsNum ? Car::hogValsNum : Car::histValsNum)*3 + 1;
        struct feature_node y[u];
        Car::x.reserve(u);
        Car::x.assign(&y[0], &y[0] + u);
    }
}

/**
 * Returns random combined (side + front) car image. 
 * @return 
 */
Mat Car::getRandomMat() {
    return combinedVec[myRandom(0, combinedVec.size())]->getCombined();
}

/**
 * Returns the sharpest combined image.
 * @return 
 */
Mat Car::getSharpestMat() {
    return sharp.back()->getCombined();
}

/**
 * Returns random combined class. 
 * @return 
 */
Combined * Car::getRandomCombined() {
    //randomXY(0, combinedVec.size() - 1)
    return combinedVec[myRandom(0, combinedVec.size())];
}

/**
 * Returns random car colour histogram vector.
 * @return 
 */
vector<float> * Car::getRandomHistVec() {
    return combinedVec[myRandom(0, combinedVec.size())]->getHistVec();
}

/**
 * Returns random car HOG vector.
 * @return 
 */
vector<float> * Car::getRandomHogVec() {
    return combinedVec[myRandom(0, combinedVec.size())]->getHogVec();
}

/**
 * Returns colour histogram feature vector from the sharpest combined image.
 * @return 
 */
vector<float> * Car::getSharpestHistVec() {
    return sharp.back()->getHistVec();
}

/**
 * Returns HOG feature vector from the sharpest combined image. 
 * @return 
 */
vector<float> * Car::getSharpestHogVec() {
    return sharp.back()->getHogVec();
}

/**
 * Adds features of car differences in the p problem. Used for training.
 * @param carX
 * @param p
 * @param HOG
 */
void Car::compareVecWith(Car* carX, Problem& p, bool HOG, double label) {
    vector<float>* vecA;
    vector<float>* vecB;
    if (!HOG) { // histogram comparison
        vecA = getRandomHistVec();
        vecB = carX->getRandomHistVec();
    } else { // HOG comparison
        vecA = getRandomHogVec();
        vecB = carX->getRandomHogVec();
    }
    p.newTd(label);
    p.endTd(addFeatures(vecA, vecB, *p.getLastXvec()));
}

/**
 * Returns side_front (number of frames inserted) size.
 * @return 
 */
int Car::getSize() {
    return (int) combinedVec.size();
}